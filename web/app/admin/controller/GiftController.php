<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
/**
 * 礼物
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class GiftController extends AdminbaseController {
    protected function getTypes($k=''){
        $type=[
            '0'=>'普通礼物',
            '1'=>'豪华礼物',
			
        ];
        if($k==''){
            return $type;
        }
        return isset($type[$k]) ? $type[$k]: '';
    }
    protected function getMark($k=''){
        $mark=[
            '0'=>'普通',
            '1'=>'热门',

        ];
        if($k==''){
            return $mark;
        }
        return isset($mark[$k]) ? $mark[$k]: '';
    }
    
    protected function getSwftype($k=''){
        $swftype=[
            '0'=>'GIF',
            '1'=>'SVGA',
        ];
        if($k==''){
            return $swftype;
        }
        return isset($swftype[$k]) ? $swftype[$k]: '';
    }
    
    public function index(){

    	$lists = Db::name("gift")
            ->where('type!=2')
			->order("list_order asc,id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['gifticon']=get_upload_path($v['gifticon']);
			$v['swf']=get_upload_path($v['swf']);
            return $v;           
        });
        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
    	$this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
    	
    	return $this->fetch();
    }
    
	public function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('gift')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }



       
        $this->resetcache();
        $this->success("删除成功！");
        
	}
    

    
    //排序
    public function listOrder() { 
		
        $model = DB::name('gift');
        parent::listOrders($model);
        
      
        
        $this->resetcache();
        $this->success("排序更新成功！");
        
    }

    public function add(){
        
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        
        return $this->fetch();				
    }

	public function addPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
            $giftname=$data['giftname'];
            if($giftname == ''){
                $this->error('请输入名称');
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}'")->find();
                if($check){
                    $this->error('名称已存在');
                }
            }
            
            
            $needcoin=$data['needcoin'];
            $gifticon=$data['gifticon'];
            
            if($needcoin==''){
                $this->error('请输入价格');
            }

            if(!is_numeric($needcoin)){
                $this->error('价格必须为数字');
            }

            if($needcoin<1){
                $this->error('价格必须为大于1的整数');
            }

            if(!$needcoin){
                $this->error('价格必须为大于1的整数');
            }

            if(floor($needcoin)!=$needcoin){
                $this->error('价格必须为大于1的整数');
            }

            if($gifticon==''){
                $this->error('请上传图片');
            }
            
            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            
            if($data['type']==1 && $data['swf']==''){
                $this->error('请上传动画效果');
            }

            $data['gifticon']=set_upload_path($data['gifticon']);
            if($data['gif']){
                $data['gif']=set_upload_path($data['gif']);
            }
            if($data['svga']){
                $data['svga']=set_upload_path($data['svga']);
            }
            
            $data['addtime']=time();
            unset($data['gif']);
            unset($data['svga']);
            
			$id = DB::name('gift')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            
            
            
            $this->resetcache();
            $this->success("添加成功！");
            
		}			
	}
    
    public function edit(){

        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('gift')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign("type", $this->getTypes());
    	$this->assign("mark", $this->getMark());
    	$this->assign("swftype", $this->getSwftype());
        
        $this->assign('data', $data);
        return $this->fetch();            
    }
    
	public function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();

            $id=$data['id'];
            $giftname=$data['giftname'];
            if($giftname == ''){
                $this->error('请输入名称');
            }else{
                $check = Db::name('gift')->where("giftname='{$giftname}' and id!={$id}")->find();
                if($check){
                    $this->error('名称已存在');
                }
            }
            
            
            $needcoin=$data['needcoin'];
            $gifticon=$data['gifticon'];
            
            if($needcoin==''){
                $this->error('请输入价格');
            }

            if(!is_numeric($needcoin)){
                $this->error('价格必须为数字');
            }

            if($needcoin<1){
                $this->error('价格必须为大于1的整数');
            }

            if(!$needcoin){
                $this->error('价格必须为大于1的整数');
            }

            if(floor($needcoin)!=$needcoin){
                $this->error('价格必须为大于1的整数');
            }

            if($gifticon==''){
                $this->error('请上传图片');
            }

            $gifticon_old=$data['gifticon_old'];
            if($gifticon!=$gifticon_old){
                $data['gifticon']=set_upload_path($gifticon);
            }

            $gif=$data['gif'];
            if($gif){
                $gif_old=$data['gif_old'];
                if($gif!=$gif_old){
                    $data['gif']=set_upload_path($gif);
                }
            }

            $svga=$data['svga'];
            if($svga){
                $svga_old=$data['svga_old'];
                if($svga!=$svga_old){
                    $data['svga']=set_upload_path($svga);
                }
            }
            
            $swftype=$data['swftype'];
            $data['swf']=$data['gif'];
            if($swftype==1){
                $data['swf']=$data['svga'];
            }
            if($data['type']==1 && $data['swf']==''){
                $this->error('请上传动画效果');
            }
            unset($data['gif']);
            unset($data['svga']);
            unset($data['gifticon_old']);
            unset($data['gif_old']);
            unset($data['svga_old']);
            
            
            
			$rs = DB::name('gift')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
        
            
            $this->resetcache();
            $this->success("修改成功！");
		}	
	}
        
    public function resetcache(){
        $key='getGiftList';
        
		$rs=DB::name('gift')
			->field("id,type,mark,giftname,needcoin,gifticon,swftime")
            ->where('type!=2')
			->order("list_order asc,id desc")
			->select();
        if($rs){
            setcaches($key,$rs);
        }else{
			delcache($key);
		}
        return 1;
    }
}
