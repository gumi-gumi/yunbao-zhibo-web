<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
class Domain_Home {

    public function getSlide($where) {
        $rs = array();
        $model = new Model_Home();
        $rs = $model->getSlide($where);
        return $rs;
    }
		

	
	public function getHot($p) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getHot($p);
				
        return $rs;
    }
		
		
	public function search($uid,$key,$p) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->search($uid,$key,$p);
				
        return $rs;
    }	
	

    public function getClassLive($liveclassid,$p){
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getClassLive($liveclassid,$p);
                
        return $rs;
    }
	

	



	

}
