<?php
// +—————————————————————————————————————————————————————————————————————
// | Created by Yunbao
// +—————————————————————————————————————————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +—————————————————————————————————————————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +—————————————————————————————————————————————————————————————————————
// | Date: 2022-02-17
// +—————————————————————————————————————————————————————————————————————
class Domain_User {

	public function getBaseInfo($userId) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->getBaseInfo($userId);

			return $rs;
	}
	
	public function checkName($uid,$name) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->checkName($uid,$name);

			return $rs;
	}
	
	public function userUpdate($uid,$fields) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->userUpdate($uid,$fields);

			return $rs;
	}
	
	public function updatePass($uid,$oldpass,$pass) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->updatePass($uid,$oldpass,$pass);

			return $rs;
	}

	public function getBalance($uid) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->getBalance($uid);

			return $rs;
	}
	
	public function getChargeRules() {
			$rs = array();

			$model = new Model_User();
			$rs = $model->getChargeRules();

			return $rs;
	}
	
	public function getProfit($uid) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->getProfit($uid);

			return $rs;
	}

	public function setCash($data) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->setCash($data);

			return $rs;
	}
	




	public function getLiverecord($touid,$p) {
			$rs = array();

			$model = new Model_User();
			$rs = $model->getLiverecord($touid,$p);

			return $rs;
	}
	
	public function getUserHome($uid,$touid) {
		$rs = array();

		$model = new Model_User();
		$rs = $model->getUserHome($uid,$touid);
		return $rs;
	}	
	


	public function getPerSetting() {
        $rs = array();
                
        $model = new Model_User();
        $rs = $model->getPerSetting();

        return $rs;
    }	

	public function getUserAccountList($uid) {
        $rs = array();
                
        $model = new Model_User();
        $rs = $model->getUserAccountList($uid);

        return $rs;
    }	

	public function getUserAccount($where) {
        $rs = array();
                
        $model = new Model_User();
        $rs = $model->getUserAccount($where);

        return $rs;
    }	

	public function setUserAccount($data) {
        $rs = array();
                
        $model = new Model_User();
        $rs = $model->setUserAccount($data);

        return $rs;
    }

	public function delUserAccount($data) {
        $rs = array();
                
        $model = new Model_User();
        $rs = $model->delUserAccount($data);

        return $rs;
    }	
	
	


	public function getAuthInfo($uid){
		$rs = array();

		$model = new Model_User();
		$rs = $model->getAuthInfo($uid);

		return $rs;
	}




}
