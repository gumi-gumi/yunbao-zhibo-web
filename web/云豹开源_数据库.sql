/*
 Navicat MySQL Data Transfer

 Source Server         : 0-公司开发服务器-86
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : 81.70.181.86:3306
 Source Schema         : ybkaiyuan

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 21/02/2022 14:44:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cmf_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `cmf_admin_menu`;
CREATE TABLE `cmf_admin_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父菜单id',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '菜单类型;1:有界面可访问菜单,2:无界面可访问菜单,0:只作为菜单',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态;1:显示,0:不显示',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `app` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应用名',
  `controller` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '控制器名',
  `action` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作名称',
  `param` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '额外参数',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `controller`(`controller`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 530 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_admin_menu
-- ----------------------------
INSERT INTO `cmf_admin_menu` VALUES (1, 0, 0, 1, 10000, 'admin', 'Plugin', 'default', '', '插件中心', 'cloud', '插件中心');
INSERT INTO `cmf_admin_menu` VALUES (6, 0, 0, 1, 3, 'admin', 'Setting', 'default', '', '设置', 'cogs', '系统设置入口');
INSERT INTO `cmf_admin_menu` VALUES (7, 6, 1, 0, 50, 'admin', 'Link', 'index', '', '友情链接', '', '友情链接管理');
INSERT INTO `cmf_admin_menu` VALUES (8, 7, 1, 0, 10000, 'admin', 'Link', 'add', '', '添加友情链接', '', '添加友情链接');
INSERT INTO `cmf_admin_menu` VALUES (9, 7, 2, 0, 10000, 'admin', 'Link', 'addPost', '', '添加友情链接提交保存', '', '添加友情链接提交保存');
INSERT INTO `cmf_admin_menu` VALUES (10, 7, 1, 0, 10000, 'admin', 'Link', 'edit', '', '编辑友情链接', '', '编辑友情链接');
INSERT INTO `cmf_admin_menu` VALUES (11, 7, 2, 0, 10000, 'admin', 'Link', 'editPost', '', '编辑友情链接提交保存', '', '编辑友情链接提交保存');
INSERT INTO `cmf_admin_menu` VALUES (12, 7, 2, 0, 10000, 'admin', 'Link', 'delete', '', '删除友情链接', '', '删除友情链接');
INSERT INTO `cmf_admin_menu` VALUES (13, 7, 2, 0, 10000, 'admin', 'Link', 'listOrder', '', '友情链接排序', '', '友情链接排序');
INSERT INTO `cmf_admin_menu` VALUES (14, 7, 2, 0, 10000, 'admin', 'Link', 'toggle', '', '友情链接显示隐藏', '', '友情链接显示隐藏');
INSERT INTO `cmf_admin_menu` VALUES (20, 0, 1, 0, 10000, 'admin', 'Menu', 'index', '', '后台菜单', '', '后台菜单管理');
INSERT INTO `cmf_admin_menu` VALUES (21, 20, 1, 0, 10000, 'admin', 'Menu', 'lists', '', '所有菜单', '', '后台所有菜单列表');
INSERT INTO `cmf_admin_menu` VALUES (22, 20, 1, 0, 10000, 'admin', 'Menu', 'add', '', '后台菜单添加', '', '后台菜单添加');
INSERT INTO `cmf_admin_menu` VALUES (23, 20, 2, 0, 10000, 'admin', 'Menu', 'addPost', '', '后台菜单添加提交保存', '', '后台菜单添加提交保存');
INSERT INTO `cmf_admin_menu` VALUES (24, 20, 1, 0, 10000, 'admin', 'Menu', 'edit', '', '后台菜单编辑', '', '后台菜单编辑');
INSERT INTO `cmf_admin_menu` VALUES (25, 20, 2, 0, 10000, 'admin', 'Menu', 'editPost', '', '后台菜单编辑提交保存', '', '后台菜单编辑提交保存');
INSERT INTO `cmf_admin_menu` VALUES (26, 20, 2, 0, 10000, 'admin', 'Menu', 'delete', '', '后台菜单删除', '', '后台菜单删除');
INSERT INTO `cmf_admin_menu` VALUES (27, 20, 2, 0, 10000, 'admin', 'Menu', 'listOrder', '', '后台菜单排序', '', '后台菜单排序');
INSERT INTO `cmf_admin_menu` VALUES (28, 20, 1, 0, 10000, 'admin', 'Menu', 'getActions', '', '导入新后台菜单', '', '导入新后台菜单');
INSERT INTO `cmf_admin_menu` VALUES (42, 1, 1, 1, 10000, 'admin', 'Plugin', 'index', '', '插件列表', '', '插件列表');
INSERT INTO `cmf_admin_menu` VALUES (43, 42, 2, 0, 10000, 'admin', 'Plugin', 'toggle', '', '插件启用禁用', '', '插件启用禁用');
INSERT INTO `cmf_admin_menu` VALUES (44, 42, 1, 0, 10000, 'admin', 'Plugin', 'setting', '', '插件设置', '', '插件设置');
INSERT INTO `cmf_admin_menu` VALUES (45, 42, 2, 0, 10000, 'admin', 'Plugin', 'settingPost', '', '插件设置提交', '', '插件设置提交');
INSERT INTO `cmf_admin_menu` VALUES (46, 42, 2, 0, 10000, 'admin', 'Plugin', 'install', '', '插件安装', '', '插件安装');
INSERT INTO `cmf_admin_menu` VALUES (47, 42, 2, 0, 10000, 'admin', 'Plugin', 'update', '', '插件更新', '', '插件更新');
INSERT INTO `cmf_admin_menu` VALUES (48, 42, 2, 0, 10000, 'admin', 'Plugin', 'uninstall', '', '卸载插件', '', '卸载插件');
INSERT INTO `cmf_admin_menu` VALUES (49, 110, 0, 1, 10000, 'admin', 'User', 'default', '', '管理组', '', '管理组');
INSERT INTO `cmf_admin_menu` VALUES (50, 49, 1, 1, 10000, 'admin', 'Rbac', 'index', '', '角色管理', '', '角色管理');
INSERT INTO `cmf_admin_menu` VALUES (51, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleAdd', '', '添加角色', '', '添加角色');
INSERT INTO `cmf_admin_menu` VALUES (52, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleAddPost', '', '添加角色提交', '', '添加角色提交');
INSERT INTO `cmf_admin_menu` VALUES (53, 50, 1, 0, 10000, 'admin', 'Rbac', 'roleEdit', '', '编辑角色', '', '编辑角色');
INSERT INTO `cmf_admin_menu` VALUES (54, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleEditPost', '', '编辑角色提交', '', '编辑角色提交');
INSERT INTO `cmf_admin_menu` VALUES (55, 50, 2, 0, 10000, 'admin', 'Rbac', 'roleDelete', '', '删除角色', '', '删除角色');
INSERT INTO `cmf_admin_menu` VALUES (56, 50, 1, 0, 10000, 'admin', 'Rbac', 'authorize', '', '设置角色权限', '', '设置角色权限');
INSERT INTO `cmf_admin_menu` VALUES (57, 50, 2, 0, 10000, 'admin', 'Rbac', 'authorizePost', '', '角色授权提交', '', '角色授权提交');
INSERT INTO `cmf_admin_menu` VALUES (58, 0, 1, 0, 10000, 'admin', 'RecycleBin', 'index', '', '回收站', '', '回收站');
INSERT INTO `cmf_admin_menu` VALUES (59, 58, 2, 0, 10000, 'admin', 'RecycleBin', 'restore', '', '回收站还原', '', '回收站还原');
INSERT INTO `cmf_admin_menu` VALUES (60, 58, 2, 0, 10000, 'admin', 'RecycleBin', 'delete', '', '回收站彻底删除', '', '回收站彻底删除');
INSERT INTO `cmf_admin_menu` VALUES (71, 6, 1, 1, 0, 'admin', 'Setting', 'site', '', '网站信息', '', '网站信息');
INSERT INTO `cmf_admin_menu` VALUES (72, 71, 2, 0, 10000, 'admin', 'Setting', 'sitePost', '', '网站信息设置提交', '', '网站信息设置提交');
INSERT INTO `cmf_admin_menu` VALUES (73, 0, 1, 0, 1, 'admin', 'Setting', 'password', '', '密码修改', '', '密码修改');
INSERT INTO `cmf_admin_menu` VALUES (74, 73, 2, 0, 10000, 'admin', 'Setting', 'passwordPost', '', '密码修改提交', '', '密码修改提交');
INSERT INTO `cmf_admin_menu` VALUES (75, 6, 1, 1, 10000, 'admin', 'Setting', 'upload', '', '上传设置', '', '上传设置');
INSERT INTO `cmf_admin_menu` VALUES (76, 75, 2, 0, 10000, 'admin', 'Setting', 'uploadPost', '', '上传设置提交', '', '上传设置提交');
INSERT INTO `cmf_admin_menu` VALUES (77, 6, 1, 0, 10000, 'admin', 'Setting', 'clearCache', '', '清除缓存', '', '清除缓存');
INSERT INTO `cmf_admin_menu` VALUES (78, 6, 1, 1, 40, 'admin', 'Slide', 'index', '', '幻灯片管理', '', '幻灯片管理');
INSERT INTO `cmf_admin_menu` VALUES (79, 78, 1, 0, 10000, 'admin', 'Slide', 'add', '', '添加幻灯片', '', '添加幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (80, 78, 2, 0, 10000, 'admin', 'Slide', 'addPost', '', '添加幻灯片提交', '', '添加幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (81, 78, 1, 0, 10000, 'admin', 'Slide', 'edit', '', '编辑幻灯片', '', '编辑幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (82, 78, 2, 0, 10000, 'admin', 'Slide', 'editPost', '', '编辑幻灯片提交', '', '编辑幻灯片提交');
INSERT INTO `cmf_admin_menu` VALUES (83, 78, 2, 0, 10000, 'admin', 'Slide', 'delete', '', '删除幻灯片', '', '删除幻灯片');
INSERT INTO `cmf_admin_menu` VALUES (84, 78, 1, 0, 10000, 'admin', 'SlideItem', 'index', '', '幻灯片页面列表', '', '幻灯片页面列表');
INSERT INTO `cmf_admin_menu` VALUES (85, 84, 1, 0, 10000, 'admin', 'SlideItem', 'add', '', '幻灯片页面添加', '', '幻灯片页面添加');
INSERT INTO `cmf_admin_menu` VALUES (86, 84, 2, 0, 10000, 'admin', 'SlideItem', 'addPost', '', '幻灯片页面添加提交', '', '幻灯片页面添加提交');
INSERT INTO `cmf_admin_menu` VALUES (87, 84, 1, 0, 10000, 'admin', 'SlideItem', 'edit', '', '幻灯片页面编辑', '', '幻灯片页面编辑');
INSERT INTO `cmf_admin_menu` VALUES (88, 84, 2, 0, 10000, 'admin', 'SlideItem', 'editPost', '', '幻灯片页面编辑提交', '', '幻灯片页面编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (89, 84, 2, 0, 10000, 'admin', 'SlideItem', 'delete', '', '幻灯片页面删除', '', '幻灯片页面删除');
INSERT INTO `cmf_admin_menu` VALUES (90, 84, 2, 0, 10000, 'admin', 'SlideItem', 'ban', '', '幻灯片页面隐藏', '', '幻灯片页面隐藏');
INSERT INTO `cmf_admin_menu` VALUES (91, 84, 2, 0, 10000, 'admin', 'SlideItem', 'cancelBan', '', '幻灯片页面显示', '', '幻灯片页面显示');
INSERT INTO `cmf_admin_menu` VALUES (92, 84, 2, 0, 10000, 'admin', 'SlideItem', 'listOrder', '', '幻灯片页面排序', '', '幻灯片页面排序');
INSERT INTO `cmf_admin_menu` VALUES (93, 6, 1, 0, 10000, 'admin', 'Storage', 'index', '', '文件存储', '', '文件存储');
INSERT INTO `cmf_admin_menu` VALUES (94, 93, 2, 0, 10000, 'admin', 'Storage', 'settingPost', '', '文件存储设置提交', '', '文件存储设置提交');
INSERT INTO `cmf_admin_menu` VALUES (110, 0, 0, 1, 4, 'user', 'AdminIndex', 'default', '', '用户管理', 'group', '用户管理');
INSERT INTO `cmf_admin_menu` VALUES (111, 49, 1, 1, 10000, 'admin', 'User', 'index', '', '管理员', '', '管理员管理');
INSERT INTO `cmf_admin_menu` VALUES (112, 111, 1, 0, 10000, 'admin', 'User', 'add', '', '管理员添加', '', '管理员添加');
INSERT INTO `cmf_admin_menu` VALUES (113, 111, 2, 0, 10000, 'admin', 'User', 'addPost', '', '管理员添加提交', '', '管理员添加提交');
INSERT INTO `cmf_admin_menu` VALUES (114, 111, 1, 0, 10000, 'admin', 'User', 'edit', '', '管理员编辑', '', '管理员编辑');
INSERT INTO `cmf_admin_menu` VALUES (115, 111, 2, 0, 10000, 'admin', 'User', 'editPost', '', '管理员编辑提交', '', '管理员编辑提交');
INSERT INTO `cmf_admin_menu` VALUES (116, 111, 1, 0, 10000, 'admin', 'User', 'userInfo', '', '个人信息', '', '管理员个人信息修改');
INSERT INTO `cmf_admin_menu` VALUES (117, 111, 2, 0, 10000, 'admin', 'User', 'userInfoPost', '', '管理员个人信息修改提交', '', '管理员个人信息修改提交');
INSERT INTO `cmf_admin_menu` VALUES (118, 111, 2, 0, 10000, 'admin', 'User', 'delete', '', '管理员删除', '', '管理员删除');
INSERT INTO `cmf_admin_menu` VALUES (119, 111, 2, 0, 10000, 'admin', 'User', 'ban', '', '停用管理员', '', '停用管理员');
INSERT INTO `cmf_admin_menu` VALUES (120, 111, 2, 0, 10000, 'admin', 'User', 'cancelBan', '', '启用管理员', '', '启用管理员');
INSERT INTO `cmf_admin_menu` VALUES (121, 0, 1, 0, 10000, 'user', 'AdminAsset', 'index', '', '资源管理', 'file', '资源管理列表');
INSERT INTO `cmf_admin_menu` VALUES (122, 121, 2, 0, 10000, 'user', 'AdminAsset', 'delete', '', '删除文件', '', '删除文件');
INSERT INTO `cmf_admin_menu` VALUES (123, 110, 0, 1, 10000, 'user', 'AdminIndex', 'default1', '', '用户组', '', '用户组');
INSERT INTO `cmf_admin_menu` VALUES (124, 123, 1, 1, 1, 'user', 'AdminIndex', 'index', '', '本站用户', '', '本站用户');
INSERT INTO `cmf_admin_menu` VALUES (144, 6, 1, 1, 1, 'admin', 'setting', 'configpri', '', '私密设置', '', '');
INSERT INTO `cmf_admin_menu` VALUES (145, 144, 1, 0, 10000, 'admin', 'setting', 'configpriPost', '', '提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (154, 0, 1, 1, 7, 'admin', 'live', 'default', '', '直播管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (155, 154, 1, 1, 1, 'admin', 'liveclass', 'index', '', '直播分类', '', '');
INSERT INTO `cmf_admin_menu` VALUES (156, 155, 1, 0, 10000, 'admin', 'liveclass', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (157, 155, 1, 0, 10000, 'admin', 'liveclass', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (158, 155, 1, 0, 10000, 'admin', 'liveclass', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (159, 155, 1, 0, 10000, 'admin', 'liveclass', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (160, 155, 1, 0, 10000, 'admin', 'liveclass', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (161, 155, 1, 0, 10000, 'admin', 'liveclass', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (168, 154, 1, 1, 5, 'admin', 'liveing', 'index', '', '直播列表', '', '');
INSERT INTO `cmf_admin_menu` VALUES (169, 168, 1, 0, 10000, 'admin', 'liveing', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (170, 168, 1, 0, 10000, 'admin', 'liveing', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (171, 168, 1, 0, 10000, 'admin', 'liveing', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (172, 168, 1, 0, 10000, 'admin', 'liveing', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (173, 168, 1, 0, 10000, 'admin', 'liveing', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (174, 154, 1, 1, 6, 'admin', 'monitor', 'index', '', '直播监控', '', '');
INSERT INTO `cmf_admin_menu` VALUES (175, 174, 1, 0, 10000, 'admin', 'monitor', 'full', '', '大屏', '', '');
INSERT INTO `cmf_admin_menu` VALUES (176, 174, 1, 0, 10000, 'admin', 'monitor', 'stop', '', '关播', '', '');
INSERT INTO `cmf_admin_menu` VALUES (177, 154, 1, 1, 7, 'admin', 'gift', 'index', '', '礼物管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (178, 177, 1, 0, 10000, 'admin', 'gift', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (179, 177, 1, 0, 10000, 'admin', 'gift', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (180, 177, 1, 0, 10000, 'admin', 'gift', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (181, 177, 1, 0, 10000, 'admin', 'gift', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (182, 177, 1, 0, 10000, 'admin', 'gift', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (183, 177, 1, 0, 10000, 'admin', 'gift', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (194, 154, 1, 1, 10, 'admin', 'liverecord', 'index', '', '直播记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (232, 0, 1, 1, 14, 'admin', 'level', 'default', '', '等级管理', 'level-up', '');
INSERT INTO `cmf_admin_menu` VALUES (233, 232, 1, 1, 10000, 'admin', 'level', 'index', '', '用户等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (234, 233, 1, 0, 10000, 'admin', 'level', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (235, 233, 1, 0, 10000, 'admin', 'level', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (236, 233, 1, 0, 10000, 'admin', 'level', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (237, 233, 1, 0, 10000, 'admin', 'level', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (238, 233, 1, 0, 10000, 'admin', 'level', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (239, 232, 1, 1, 10000, 'admin', 'levelanchor', 'index', '', '主播等级', '', '');
INSERT INTO `cmf_admin_menu` VALUES (240, 239, 1, 0, 10000, 'admin', 'levelanchor', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (241, 239, 1, 0, 10000, 'admin', 'levelanchor', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (242, 239, 1, 0, 10000, 'admin', 'levelanchor', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (243, 239, 1, 0, 10000, 'admin', 'levelanchor', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (244, 239, 1, 0, 10000, 'admin', 'levelanchor', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (279, 0, 1, 1, 9, 'admin', 'finance', 'default', '', '财务管理', 'rmb', '');
INSERT INTO `cmf_admin_menu` VALUES (280, 279, 1, 1, 1, 'admin', 'chargerules', 'index', '', '充值规则', '', '');
INSERT INTO `cmf_admin_menu` VALUES (281, 280, 1, 0, 10000, 'admin', 'chargerules', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (282, 280, 1, 0, 10000, 'admin', 'chargerules', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (283, 280, 1, 0, 10000, 'admin', 'chargerules', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (284, 280, 1, 0, 10000, 'admin', 'chargerules', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (285, 280, 1, 0, 10000, 'admin', 'chargerules', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (286, 280, 1, 0, 10000, 'admin', 'chargerules', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (287, 279, 1, 1, 2, 'admin', 'charge', 'index', '', '钻石充值记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (288, 287, 1, 0, 10000, 'admin', 'charge', 'setpay', '', '确认支付', '', '');
INSERT INTO `cmf_admin_menu` VALUES (289, 287, 1, 0, 10000, 'admin', 'charge', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (290, 279, 1, 1, 3, 'admin', 'manual', 'index', '', '手动充值钻石', '', '');
INSERT INTO `cmf_admin_menu` VALUES (291, 290, 1, 0, 10000, 'admin', 'manual', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (292, 290, 1, 0, 10000, 'admin', 'manual', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (293, 290, 1, 0, 10000, 'admin', 'manual', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (294, 279, 1, 1, 4, 'admin', 'coinrecord', 'index', '', '钻石消费记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (295, 279, 1, 1, 5, 'admin', 'cash', 'index', '', '映票提现记录', '', '');
INSERT INTO `cmf_admin_menu` VALUES (296, 295, 1, 0, 10000, 'admin', 'cash', 'export', '', '导出', '', '');
INSERT INTO `cmf_admin_menu` VALUES (297, 295, 1, 0, 10000, 'admin', 'cash', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (298, 295, 1, 0, 10000, 'admin', 'cash', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (299, 6, 1, 1, 10000, 'admin', 'guide', 'set', '', '引导页', '', '');
INSERT INTO `cmf_admin_menu` VALUES (300, 299, 1, 0, 10000, 'admin', 'guide', 'setPost', '', '设置提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (301, 299, 1, 0, 10000, 'admin', 'guide', 'index', '', '管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (302, 301, 1, 0, 10000, 'admin', 'guide', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (303, 301, 1, 0, 10000, 'admin', 'guide', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (304, 301, 1, 0, 10000, 'admin', 'guide', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (305, 301, 1, 0, 10000, 'admin', 'guide', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (306, 301, 1, 0, 10000, 'admin', 'guide', 'listOrder', '', '排序', '', '');
INSERT INTO `cmf_admin_menu` VALUES (307, 301, 1, 0, 10000, 'admin', 'guide', 'del', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (308, 0, 1, 1, 6, 'admin', 'auth', 'index', '', '身份认证', '', '');
INSERT INTO `cmf_admin_menu` VALUES (310, 308, 1, 0, 10000, 'admin', 'auth', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (311, 308, 1, 0, 10000, 'admin', 'auth', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (336, 0, 1, 1, 19, 'admin', 'note', 'default', '', '消息管理', 'bell', '');
INSERT INTO `cmf_admin_menu` VALUES (344, 336, 1, 1, 10000, 'admin', 'system', 'index', '', '直播间消息', '', '');
INSERT INTO `cmf_admin_menu` VALUES (345, 344, 1, 0, 10000, 'admin', 'system', 'send', '', '发送', '', '');
INSERT INTO `cmf_admin_menu` VALUES (353, 0, 1, 1, 20, 'admin', 'portal', 'default', '', '内容管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (357, 353, 1, 1, 10000, 'portal', 'AdminPage', 'index', '', '页面管理', '', '');
INSERT INTO `cmf_admin_menu` VALUES (358, 357, 1, 0, 10000, 'portal', 'AdminPage', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (359, 357, 1, 0, 10000, 'portal', 'AdminPage', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (360, 357, 1, 0, 10000, 'portal', 'AdminPage', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (361, 357, 1, 0, 10000, 'portal', 'AdminPage', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (362, 357, 1, 0, 10000, 'portal', 'AdminPage', 'delete', '', '删除', '', '');
INSERT INTO `cmf_admin_menu` VALUES (404, 124, 1, 0, 10000, 'user', 'AdminIndex', 'add', '', '添加', '', '');
INSERT INTO `cmf_admin_menu` VALUES (405, 124, 1, 0, 10000, 'user', 'AdminIndex', 'addPost', '', '添加提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (406, 124, 1, 0, 10000, 'user', 'AdminIndex', 'edit', '', '编辑', '', '');
INSERT INTO `cmf_admin_menu` VALUES (407, 124, 1, 0, 10000, 'user', 'AdminIndex', 'editPost', '', '编辑提交', '', '');
INSERT INTO `cmf_admin_menu` VALUES (410, 124, 1, 0, 10000, 'user', 'AdminIndex', 'sethot', '', '设置取消热门', '', '');
INSERT INTO `cmf_admin_menu` VALUES (504, 124, 1, 0, 10000, 'user', 'AdminIndex', 'del', '', '本站用户删除', '', '');

-- ----------------------------
-- Table structure for cmf_asset
-- ----------------------------
DROP TABLE IF EXISTS `cmf_asset`;
CREATE TABLE `cmf_asset`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `file_size` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小,单位B',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传时间',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:可用,0:不可用',
  `download_times` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下载次数',
  `file_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件惟一码',
  `filename` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `file_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件路径,相对于upload目录,可以为url',
  `file_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件md5值',
  `file_sha1` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `suffix` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件后缀名,不包括点',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '其它详细信息,JSON格式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_asset
-- ----------------------------
INSERT INTO `cmf_asset` VALUES (1, 1, 34577, 1645153613, 1, 0, '9053a33871cfef420c7514bd56265f8aba64a19e9fd5adfbd99ca1a08ced01ec', '0d47ee72f8fb5a76e9ff7e9a8a91f8f3.png', 'admin/20220218/bd96c2bac74a5269177520d442590f63.png', '9053a33871cfef420c7514bd56265f8a', 'a479c44cfa32610f48d3927695682664d835a1a3', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (2, 1, 4760, 1645232518, 1, 0, '3798f868371453360228b1e919a3a2f4604ace69b6a206cd788d5941a16041b4', 'liveclass_2.png', 'admin/20220219/3135bab354188b0cfa1d94087e568a66.png', '3798f868371453360228b1e919a3a2f4', '5999d8c63f97456caf9f08c292a38baec88add86', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (3, 1, 4494, 1645232558, 1, 0, '12fa726e981184f26f555af868b8651b84f98ecab8d0d91790abc1ef41ef60e3', 'liveclass_11.png', 'admin/20220219/f24f63e8be1043769f51091d5bf6eba6.png', '12fa726e981184f26f555af868b8651b', 'e19059ace2907d95497e439d3343e71bcbfade37', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (4, 1, 34710, 1645233249, 1, 0, '6dfad7c6cacede0ad7f6ec53dc95777b98b7c5be136f35c58454f3e62d44cb3d', '首页banner.jpg', 'admin/20220219/6097e26083e49afcf51920241a77804c.jpg', '6dfad7c6cacede0ad7f6ec53dc95777b', 'cbebd8ce1571b254355408a734093f4b00a03033', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (5, 1, 37380, 1645233666, 1, 0, 'fe44b01a42b53ac72f3beb0faad167a5fba207c5f7cc451c8ef40e0bf0483df6', '首页banner.jpg', 'admin/20220219/6ad916d6749af54f8df7f42cb8f6db48.jpg', 'fe44b01a42b53ac72f3beb0faad167a5', 'a881ee18868ce8a193efd35b600cdb9e18ed5f06', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (6, 1, 24876, 1645238486, 1, 0, 'ce7dcf364d12513e45b66ddbe2b062d6c41678052ffde40c335e221fc0d9cb9e', '1-4.jpg', 'admin/20220219/83ae6d314fb0206d2116917958a0f2de.jpg', 'ce7dcf364d12513e45b66ddbe2b062d6', 'c459da3b9d84b4a4761577beabd015ecd122884f', 'jpg', NULL);
INSERT INTO `cmf_asset` VALUES (7, 1, 19945, 1645238633, 1, 0, 'c4b8b0839b0da37afb91ada874b868c2d90d26cab6849b3ac2ad34c55cdb61a6', 'gift_33.png', 'admin/20220219/1deb65fcb7ecca0e23d5cdef68ea50e2.png', 'c4b8b0839b0da37afb91ada874b868c2', '6596702d9fa1c113ae919195a3f7ffa2fb33a80a', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (8, 1, 28957, 1645238660, 1, 0, '24d4f6a5ba63aa668a9985192dab2acd9722748d2ac1d9850144d16dddc816df', 'gift_11.png', 'admin/20220219/067ebd4642bb0875541ef5cadef59e62.png', '24d4f6a5ba63aa668a9985192dab2acd', 'a5be32e400256a34ceb104180db23da8a07295c2', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (9, 1, 23435, 1645238688, 1, 0, 'f3814b9a27e715604ad1692cc8d5317a0c353bf32220a1068ba03d6632b47788', 'gift_32.png', 'admin/20220219/67d6e5cd3a77258d7c8b5734ca7da370.png', 'f3814b9a27e715604ad1692cc8d5317a', 'b7686957098227ebd8ea886190205eae78291c1a', 'png', NULL);
INSERT INTO `cmf_asset` VALUES (10, 1, 195773, 1645241785, 1, 0, '000917a528658abcbdd2618d5e7e1eba2717654f7fa7ce263160b3656b50f070', '直播-引导页.jpg', 'admin/20220219/649fc373b079abde037f811a3719d1e5.jpg', '000917a528658abcbdd2618d5e7e1eba', '238309cb0cc4180ae192742bdea196f3852190fe', 'jpg', NULL);

-- ----------------------------
-- Table structure for cmf_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_access`;
CREATE TABLE `cmf_auth_access`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色',
  `rule_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `type` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '权限规则分类,请加应用前缀,如admin_',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限授权表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_rule`;
CREATE TABLE `cmf_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否有效(0:无效,1:有效)',
  `app` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则所属app',
  `type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '额外url参数',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '规则描述',
  `condition` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `module`(`app`, `status`, `type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 536 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限规则表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_auth_rule
-- ----------------------------
INSERT INTO `cmf_auth_rule` VALUES (1, 1, 'admin', 'admin_url', 'admin/Hook/index', '', '钩子管理', '');
INSERT INTO `cmf_auth_rule` VALUES (2, 1, 'admin', 'admin_url', 'admin/Hook/plugins', '', '钩子插件管理', '');
INSERT INTO `cmf_auth_rule` VALUES (3, 1, 'admin', 'admin_url', 'admin/Hook/pluginListOrder', '', '钩子插件排序', '');
INSERT INTO `cmf_auth_rule` VALUES (4, 1, 'admin', 'admin_url', 'admin/Hook/sync', '', '同步钩子', '');
INSERT INTO `cmf_auth_rule` VALUES (5, 1, 'admin', 'admin_url', 'admin/Link/index', '', '友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (6, 1, 'admin', 'admin_url', 'admin/Link/add', '', '添加友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (7, 1, 'admin', 'admin_url', 'admin/Link/addPost', '', '添加友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (8, 1, 'admin', 'admin_url', 'admin/Link/edit', '', '编辑友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (9, 1, 'admin', 'admin_url', 'admin/Link/editPost', '', '编辑友情链接提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (10, 1, 'admin', 'admin_url', 'admin/Link/delete', '', '删除友情链接', '');
INSERT INTO `cmf_auth_rule` VALUES (11, 1, 'admin', 'admin_url', 'admin/Link/listOrder', '', '友情链接排序', '');
INSERT INTO `cmf_auth_rule` VALUES (12, 1, 'admin', 'admin_url', 'admin/Link/toggle', '', '友情链接显示隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (13, 1, 'admin', 'admin_url', 'admin/Mailer/index', '', '邮箱配置', '');
INSERT INTO `cmf_auth_rule` VALUES (14, 1, 'admin', 'admin_url', 'admin/Mailer/indexPost', '', '邮箱配置提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (15, 1, 'admin', 'admin_url', 'admin/Mailer/template', '', '邮件模板', '');
INSERT INTO `cmf_auth_rule` VALUES (16, 1, 'admin', 'admin_url', 'admin/Mailer/templatePost', '', '邮件模板提交', '');
INSERT INTO `cmf_auth_rule` VALUES (17, 1, 'admin', 'admin_url', 'admin/Mailer/test', '', '邮件发送测试', '');
INSERT INTO `cmf_auth_rule` VALUES (18, 1, 'admin', 'admin_url', 'admin/Menu/index', '', '后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (19, 1, 'admin', 'admin_url', 'admin/Menu/lists', '', '所有菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (20, 1, 'admin', 'admin_url', 'admin/Menu/add', '', '后台菜单添加', '');
INSERT INTO `cmf_auth_rule` VALUES (21, 1, 'admin', 'admin_url', 'admin/Menu/addPost', '', '后台菜单添加提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (22, 1, 'admin', 'admin_url', 'admin/Menu/edit', '', '后台菜单编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (23, 1, 'admin', 'admin_url', 'admin/Menu/editPost', '', '后台菜单编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (24, 1, 'admin', 'admin_url', 'admin/Menu/delete', '', '后台菜单删除', '');
INSERT INTO `cmf_auth_rule` VALUES (25, 1, 'admin', 'admin_url', 'admin/Menu/listOrder', '', '后台菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (26, 1, 'admin', 'admin_url', 'admin/Menu/getActions', '', '导入新后台菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (27, 1, 'admin', 'admin_url', 'admin/Nav/index', '', '导航管理', '');
INSERT INTO `cmf_auth_rule` VALUES (28, 1, 'admin', 'admin_url', 'admin/Nav/add', '', '添加导航', '');
INSERT INTO `cmf_auth_rule` VALUES (29, 1, 'admin', 'admin_url', 'admin/Nav/addPost', '', '添加导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (30, 1, 'admin', 'admin_url', 'admin/Nav/edit', '', '编辑导航', '');
INSERT INTO `cmf_auth_rule` VALUES (31, 1, 'admin', 'admin_url', 'admin/Nav/editPost', '', '编辑导航提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (32, 1, 'admin', 'admin_url', 'admin/Nav/delete', '', '删除导航', '');
INSERT INTO `cmf_auth_rule` VALUES (33, 1, 'admin', 'admin_url', 'admin/NavMenu/index', '', '导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (34, 1, 'admin', 'admin_url', 'admin/NavMenu/add', '', '添加导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (35, 1, 'admin', 'admin_url', 'admin/NavMenu/addPost', '', '添加导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (36, 1, 'admin', 'admin_url', 'admin/NavMenu/edit', '', '编辑导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (37, 1, 'admin', 'admin_url', 'admin/NavMenu/editPost', '', '编辑导航菜单提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (38, 1, 'admin', 'admin_url', 'admin/NavMenu/delete', '', '删除导航菜单', '');
INSERT INTO `cmf_auth_rule` VALUES (39, 1, 'admin', 'admin_url', 'admin/NavMenu/listOrder', '', '导航菜单排序', '');
INSERT INTO `cmf_auth_rule` VALUES (40, 1, 'admin', 'admin_url', 'admin/Plugin/default', '', '插件中心', '');
INSERT INTO `cmf_auth_rule` VALUES (41, 1, 'admin', 'admin_url', 'admin/Plugin/index', '', '插件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (42, 1, 'admin', 'admin_url', 'admin/Plugin/toggle', '', '插件启用禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (43, 1, 'admin', 'admin_url', 'admin/Plugin/setting', '', '插件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (44, 1, 'admin', 'admin_url', 'admin/Plugin/settingPost', '', '插件设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (45, 1, 'admin', 'admin_url', 'admin/Plugin/install', '', '插件安装', '');
INSERT INTO `cmf_auth_rule` VALUES (46, 1, 'admin', 'admin_url', 'admin/Plugin/update', '', '插件更新', '');
INSERT INTO `cmf_auth_rule` VALUES (47, 1, 'admin', 'admin_url', 'admin/Plugin/uninstall', '', '卸载插件', '');
INSERT INTO `cmf_auth_rule` VALUES (48, 1, 'admin', 'admin_url', 'admin/Rbac/index', '', '角色管理', '');
INSERT INTO `cmf_auth_rule` VALUES (49, 1, 'admin', 'admin_url', 'admin/Rbac/roleAdd', '', '添加角色', '');
INSERT INTO `cmf_auth_rule` VALUES (50, 1, 'admin', 'admin_url', 'admin/Rbac/roleAddPost', '', '添加角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (51, 1, 'admin', 'admin_url', 'admin/Rbac/roleEdit', '', '编辑角色', '');
INSERT INTO `cmf_auth_rule` VALUES (52, 1, 'admin', 'admin_url', 'admin/Rbac/roleEditPost', '', '编辑角色提交', '');
INSERT INTO `cmf_auth_rule` VALUES (53, 1, 'admin', 'admin_url', 'admin/Rbac/roleDelete', '', '删除角色', '');
INSERT INTO `cmf_auth_rule` VALUES (54, 1, 'admin', 'admin_url', 'admin/Rbac/authorize', '', '设置角色权限', '');
INSERT INTO `cmf_auth_rule` VALUES (55, 1, 'admin', 'admin_url', 'admin/Rbac/authorizePost', '', '角色授权提交', '');
INSERT INTO `cmf_auth_rule` VALUES (56, 1, 'admin', 'admin_url', 'admin/RecycleBin/index', '', '回收站', '');
INSERT INTO `cmf_auth_rule` VALUES (57, 1, 'admin', 'admin_url', 'admin/RecycleBin/restore', '', '回收站还原', '');
INSERT INTO `cmf_auth_rule` VALUES (58, 1, 'admin', 'admin_url', 'admin/RecycleBin/delete', '', '回收站彻底删除', '');
INSERT INTO `cmf_auth_rule` VALUES (59, 1, 'admin', 'admin_url', 'admin/Route/index', '', 'URL美化', '');
INSERT INTO `cmf_auth_rule` VALUES (60, 1, 'admin', 'admin_url', 'admin/Route/add', '', '添加路由规则', '');
INSERT INTO `cmf_auth_rule` VALUES (61, 1, 'admin', 'admin_url', 'admin/Route/addPost', '', '添加路由规则提交', '');
INSERT INTO `cmf_auth_rule` VALUES (62, 1, 'admin', 'admin_url', 'admin/Route/edit', '', '路由规则编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (63, 1, 'admin', 'admin_url', 'admin/Route/editPost', '', '路由规则编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (64, 1, 'admin', 'admin_url', 'admin/Route/delete', '', '路由规则删除', '');
INSERT INTO `cmf_auth_rule` VALUES (65, 1, 'admin', 'admin_url', 'admin/Route/ban', '', '路由规则禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (66, 1, 'admin', 'admin_url', 'admin/Route/open', '', '路由规则启用', '');
INSERT INTO `cmf_auth_rule` VALUES (67, 1, 'admin', 'admin_url', 'admin/Route/listOrder', '', '路由规则排序', '');
INSERT INTO `cmf_auth_rule` VALUES (68, 1, 'admin', 'admin_url', 'admin/Route/select', '', '选择URL', '');
INSERT INTO `cmf_auth_rule` VALUES (69, 1, 'admin', 'admin_url', 'admin/Setting/default', '', '设置', '');
INSERT INTO `cmf_auth_rule` VALUES (70, 1, 'admin', 'admin_url', 'admin/Setting/site', '', '网站信息', '');
INSERT INTO `cmf_auth_rule` VALUES (71, 1, 'admin', 'admin_url', 'admin/Setting/sitePost', '', '网站信息设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (72, 1, 'admin', 'admin_url', 'admin/Setting/password', '', '密码修改', '');
INSERT INTO `cmf_auth_rule` VALUES (73, 1, 'admin', 'admin_url', 'admin/Setting/passwordPost', '', '密码修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (74, 1, 'admin', 'admin_url', 'admin/Setting/upload', '', '上传设置', '');
INSERT INTO `cmf_auth_rule` VALUES (75, 1, 'admin', 'admin_url', 'admin/Setting/uploadPost', '', '上传设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (76, 1, 'admin', 'admin_url', 'admin/Setting/clearCache', '', '清除缓存', '');
INSERT INTO `cmf_auth_rule` VALUES (77, 1, 'admin', 'admin_url', 'admin/Slide/index', '', '幻灯片管理', '');
INSERT INTO `cmf_auth_rule` VALUES (78, 1, 'admin', 'admin_url', 'admin/Slide/add', '', '添加幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (79, 1, 'admin', 'admin_url', 'admin/Slide/addPost', '', '添加幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (80, 1, 'admin', 'admin_url', 'admin/Slide/edit', '', '编辑幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (81, 1, 'admin', 'admin_url', 'admin/Slide/editPost', '', '编辑幻灯片提交', '');
INSERT INTO `cmf_auth_rule` VALUES (82, 1, 'admin', 'admin_url', 'admin/Slide/delete', '', '删除幻灯片', '');
INSERT INTO `cmf_auth_rule` VALUES (83, 1, 'admin', 'admin_url', 'admin/SlideItem/index', '', '幻灯片页面列表', '');
INSERT INTO `cmf_auth_rule` VALUES (84, 1, 'admin', 'admin_url', 'admin/SlideItem/add', '', '幻灯片页面添加', '');
INSERT INTO `cmf_auth_rule` VALUES (85, 1, 'admin', 'admin_url', 'admin/SlideItem/addPost', '', '幻灯片页面添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (86, 1, 'admin', 'admin_url', 'admin/SlideItem/edit', '', '幻灯片页面编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (87, 1, 'admin', 'admin_url', 'admin/SlideItem/editPost', '', '幻灯片页面编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (88, 1, 'admin', 'admin_url', 'admin/SlideItem/delete', '', '幻灯片页面删除', '');
INSERT INTO `cmf_auth_rule` VALUES (89, 1, 'admin', 'admin_url', 'admin/SlideItem/ban', '', '幻灯片页面隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (90, 1, 'admin', 'admin_url', 'admin/SlideItem/cancelBan', '', '幻灯片页面显示', '');
INSERT INTO `cmf_auth_rule` VALUES (91, 1, 'admin', 'admin_url', 'admin/SlideItem/listOrder', '', '幻灯片页面排序', '');
INSERT INTO `cmf_auth_rule` VALUES (92, 1, 'admin', 'admin_url', 'admin/Storage/index', '', '文件存储', '');
INSERT INTO `cmf_auth_rule` VALUES (93, 1, 'admin', 'admin_url', 'admin/Storage/settingPost', '', '文件存储设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (94, 1, 'admin', 'admin_url', 'admin/Theme/index', '', '模板管理', '');
INSERT INTO `cmf_auth_rule` VALUES (95, 1, 'admin', 'admin_url', 'admin/Theme/install', '', '安装模板', '');
INSERT INTO `cmf_auth_rule` VALUES (96, 1, 'admin', 'admin_url', 'admin/Theme/uninstall', '', '卸载模板', '');
INSERT INTO `cmf_auth_rule` VALUES (97, 1, 'admin', 'admin_url', 'admin/Theme/installTheme', '', '模板安装', '');
INSERT INTO `cmf_auth_rule` VALUES (98, 1, 'admin', 'admin_url', 'admin/Theme/update', '', '模板更新', '');
INSERT INTO `cmf_auth_rule` VALUES (99, 1, 'admin', 'admin_url', 'admin/Theme/active', '', '启用模板', '');
INSERT INTO `cmf_auth_rule` VALUES (100, 1, 'admin', 'admin_url', 'admin/Theme/files', '', '模板文件列表', '');
INSERT INTO `cmf_auth_rule` VALUES (101, 1, 'admin', 'admin_url', 'admin/Theme/fileSetting', '', '模板文件设置', '');
INSERT INTO `cmf_auth_rule` VALUES (102, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayData', '', '模板文件数组数据列表', '');
INSERT INTO `cmf_auth_rule` VALUES (103, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEdit', '', '模板文件数组数据添加编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (104, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataEditPost', '', '模板文件数组数据添加编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (105, 1, 'admin', 'admin_url', 'admin/Theme/fileArrayDataDelete', '', '模板文件数组数据删除', '');
INSERT INTO `cmf_auth_rule` VALUES (106, 1, 'admin', 'admin_url', 'admin/Theme/settingPost', '', '模板文件编辑提交保存', '');
INSERT INTO `cmf_auth_rule` VALUES (107, 1, 'admin', 'admin_url', 'admin/Theme/dataSource', '', '模板文件设置数据源', '');
INSERT INTO `cmf_auth_rule` VALUES (108, 1, 'admin', 'admin_url', 'admin/Theme/design', '', '模板设计', '');
INSERT INTO `cmf_auth_rule` VALUES (109, 1, 'admin', 'admin_url', 'admin/User/default', '', '管理组', '');
INSERT INTO `cmf_auth_rule` VALUES (110, 1, 'admin', 'admin_url', 'admin/User/index', '', '管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (111, 1, 'admin', 'admin_url', 'admin/User/add', '', '管理员添加', '');
INSERT INTO `cmf_auth_rule` VALUES (112, 1, 'admin', 'admin_url', 'admin/User/addPost', '', '管理员添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (113, 1, 'admin', 'admin_url', 'admin/User/edit', '', '管理员编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (114, 1, 'admin', 'admin_url', 'admin/User/editPost', '', '管理员编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (115, 1, 'admin', 'admin_url', 'admin/User/userInfo', '', '个人信息', '');
INSERT INTO `cmf_auth_rule` VALUES (116, 1, 'admin', 'admin_url', 'admin/User/userInfoPost', '', '管理员个人信息修改提交', '');
INSERT INTO `cmf_auth_rule` VALUES (117, 1, 'admin', 'admin_url', 'admin/User/delete', '', '管理员删除', '');
INSERT INTO `cmf_auth_rule` VALUES (118, 1, 'admin', 'admin_url', 'admin/User/ban', '', '停用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (119, 1, 'admin', 'admin_url', 'admin/User/cancelBan', '', '启用管理员', '');
INSERT INTO `cmf_auth_rule` VALUES (120, 1, 'user', 'admin_url', 'user/AdminAsset/index', '', '资源管理', '');
INSERT INTO `cmf_auth_rule` VALUES (121, 1, 'user', 'admin_url', 'user/AdminAsset/delete', '', '删除文件', '');
INSERT INTO `cmf_auth_rule` VALUES (122, 1, 'user', 'admin_url', 'user/AdminIndex/default', '', '用户管理', '');
INSERT INTO `cmf_auth_rule` VALUES (123, 1, 'user', 'admin_url', 'user/AdminIndex/default1', '', '用户组', '');
INSERT INTO `cmf_auth_rule` VALUES (124, 1, 'user', 'admin_url', 'user/AdminIndex/index', '', '本站用户', '');
INSERT INTO `cmf_auth_rule` VALUES (125, 1, 'user', 'admin_url', 'user/AdminIndex/ban', '', '本站用户拉黑', '');
INSERT INTO `cmf_auth_rule` VALUES (126, 1, 'user', 'admin_url', 'user/AdminIndex/cancelBan', '', '本站用户启用', '');
INSERT INTO `cmf_auth_rule` VALUES (127, 1, 'user', 'admin_url', 'user/AdminOauth/index', '', '第三方用户', '');
INSERT INTO `cmf_auth_rule` VALUES (128, 1, 'user', 'admin_url', 'user/AdminOauth/delete', '', '删除第三方用户绑定', '');
INSERT INTO `cmf_auth_rule` VALUES (129, 1, 'user', 'admin_url', 'user/AdminUserAction/index', '', '用户操作管理', '');
INSERT INTO `cmf_auth_rule` VALUES (130, 1, 'user', 'admin_url', 'user/AdminUserAction/edit', '', '编辑用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (131, 1, 'user', 'admin_url', 'user/AdminUserAction/editPost', '', '编辑用户操作提交', '');
INSERT INTO `cmf_auth_rule` VALUES (132, 1, 'user', 'admin_url', 'user/AdminUserAction/sync', '', '同步用户操作', '');
INSERT INTO `cmf_auth_rule` VALUES (133, 1, 'admin', 'admin_url', 'admin/adminlog/index', '', '操作日志', '');
INSERT INTO `cmf_auth_rule` VALUES (134, 1, 'admin', 'admin_url', 'admin/adminlog/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (135, 1, 'admin', 'admin_url', 'admin/adminlog/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (136, 1, 'admin', 'admin_url', 'admin/Agent/default', '', '邀请奖励', '');
INSERT INTO `cmf_auth_rule` VALUES (137, 1, 'admin', 'admin_url', 'admin/agent/index', '', '邀请关系', '');
INSERT INTO `cmf_auth_rule` VALUES (138, 1, 'admin', 'admin_url', 'admin/agent/index2', '', '邀请收益', '');
INSERT INTO `cmf_auth_rule` VALUES (139, 1, 'admin', 'admin_url', 'admin/car/default', '', '道具管理', '');
INSERT INTO `cmf_auth_rule` VALUES (140, 1, 'admin', 'admin_url', 'admin/car/index', '', '坐骑管理', '');
INSERT INTO `cmf_auth_rule` VALUES (141, 1, 'admin', 'admin_url', 'admin/car/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (142, 1, 'admin', 'admin_url', 'admin/car/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (143, 1, 'admin', 'admin_url', 'admin/car/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (144, 1, 'admin', 'admin_url', 'admin/car/editPost', '', '编辑添加', '');
INSERT INTO `cmf_auth_rule` VALUES (145, 1, 'admin', 'admin_url', 'admin/car/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (146, 1, 'admin', 'admin_url', 'admin/car/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (147, 1, 'admin', 'admin_url', 'admin/liang/index', '', '靓号管理', '');
INSERT INTO `cmf_auth_rule` VALUES (148, 1, 'admin', 'admin_url', 'admin/liang/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (149, 1, 'admin', 'admin_url', 'admin/liang/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (150, 1, 'admin', 'admin_url', 'admin/setting/configpri', '', '私密设置', '');
INSERT INTO `cmf_auth_rule` VALUES (151, 1, 'admin', 'admin_url', 'admin/setting/configpriPost', '', '提交', '');
INSERT INTO `cmf_auth_rule` VALUES (152, 1, 'admin', 'admin_url', 'admin/jackpot/set', '', '奖池管理', '');
INSERT INTO `cmf_auth_rule` VALUES (153, 1, 'admin', 'admin_url', 'admin/jackpot/setPost', '', '设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (154, 1, 'admin', 'admin_url', 'admin/jackpot/index', '', '奖池等级', '');
INSERT INTO `cmf_auth_rule` VALUES (155, 1, 'admin', 'admin_url', 'admin/jackpot/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (156, 1, 'admin', 'admin_url', 'admin/jackpot/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (157, 1, 'admin', 'admin_url', 'admin/jackpot/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (158, 1, 'admin', 'admin_url', 'admin/jackpot/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (159, 1, 'admin', 'admin_url', 'admin/jackpot/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (160, 1, 'admin', 'admin_url', 'admin/live/default', '', '直播管理', '');
INSERT INTO `cmf_auth_rule` VALUES (161, 1, 'admin', 'admin_url', 'admin/liveclass/index', '', '直播分类', '');
INSERT INTO `cmf_auth_rule` VALUES (162, 1, 'admin', 'admin_url', 'admin/liveclass/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (163, 1, 'admin', 'admin_url', 'admin/liveclass/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (164, 1, 'admin', 'admin_url', 'admin/liveclass/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (165, 1, 'admin', 'admin_url', 'admin/liveclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (166, 1, 'admin', 'admin_url', 'admin/liveclass/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (167, 1, 'admin', 'admin_url', 'admin/liveclass/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (168, 1, 'admin', 'admin_url', 'admin/liveban/index', '', '禁播管理', '');
INSERT INTO `cmf_auth_rule` VALUES (169, 1, 'admin', 'admin_url', 'admin/liveban/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (170, 1, 'admin', 'admin_url', 'admin/liveshut/index', '', '禁言管理', '');
INSERT INTO `cmf_auth_rule` VALUES (171, 1, 'admin', 'admin_url', 'admin/liveshut/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (172, 1, 'admin', 'admin_url', 'admin/livekick/index', '', '踢人管理', '');
INSERT INTO `cmf_auth_rule` VALUES (173, 1, 'admin', 'admin_url', 'admin/livekick/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (174, 1, 'admin', 'admin_url', 'admin/liveing/index', '', '直播列表', '');
INSERT INTO `cmf_auth_rule` VALUES (175, 1, 'admin', 'admin_url', 'admin/liveing/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (176, 1, 'admin', 'admin_url', 'admin/liveing/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (177, 1, 'admin', 'admin_url', 'admin/liveing/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (178, 1, 'admin', 'admin_url', 'admin/liveing/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (179, 1, 'admin', 'admin_url', 'admin/liveing/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (180, 1, 'admin', 'admin_url', 'admin/monitor/index', '', '直播监控', '');
INSERT INTO `cmf_auth_rule` VALUES (181, 1, 'admin', 'admin_url', 'admin/monitor/full', '', '大屏', '');
INSERT INTO `cmf_auth_rule` VALUES (182, 1, 'admin', 'admin_url', 'admin/monitor/stop', '', '关播', '');
INSERT INTO `cmf_auth_rule` VALUES (183, 1, 'admin', 'admin_url', 'admin/gift/index', '', '礼物管理', '');
INSERT INTO `cmf_auth_rule` VALUES (184, 1, 'admin', 'admin_url', 'admin/gift/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (185, 1, 'admin', 'admin_url', 'admin/gift/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (186, 1, 'admin', 'admin_url', 'admin/gift/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (187, 1, 'admin', 'admin_url', 'admin/gift/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (188, 1, 'admin', 'admin_url', 'admin/gift/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (189, 1, 'admin', 'admin_url', 'admin/gift/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (190, 1, 'admin', 'admin_url', 'admin/report/defult', '', '举报管理', '');
INSERT INTO `cmf_auth_rule` VALUES (191, 1, 'admin', 'admin_url', 'admin/reportcat/index', '', '举报分类', '');
INSERT INTO `cmf_auth_rule` VALUES (192, 1, 'admin', 'admin_url', 'admin/reportcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (193, 1, 'admin', 'admin_url', 'admin/reportcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (194, 1, 'admin', 'admin_url', 'admin/reportcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (195, 1, 'admin', 'admin_url', 'admin/reportcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (196, 1, 'admin', 'admin_url', 'admin/reportcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (197, 1, 'admin', 'admin_url', 'admin/reportcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (198, 1, 'admin', 'admin_url', 'admin/report/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (199, 1, 'admin', 'admin_url', 'admin/report/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (200, 1, 'admin', 'admin_url', 'admin/liverecord/index', '', '直播记录', '');
INSERT INTO `cmf_auth_rule` VALUES (201, 1, 'admin', 'admin_url', 'admin/video/default', '', '视频管理', '');
INSERT INTO `cmf_auth_rule` VALUES (202, 1, 'admin', 'admin_url', 'admin/music/index', '', '音乐管理', '');
INSERT INTO `cmf_auth_rule` VALUES (203, 1, 'admin', 'admin_url', 'admin/musiccat/index', '', '音乐分类', '');
INSERT INTO `cmf_auth_rule` VALUES (204, 1, 'admin', 'admin_url', 'admin/music/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (205, 1, 'admin', 'admin_url', 'admin/music/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (206, 1, 'admin', 'admin_url', 'admin/music/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (207, 1, 'admin', 'admin_url', 'admin/music/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (208, 1, 'admin', 'admin_url', 'admin/music/listen', '', '试听', '');
INSERT INTO `cmf_auth_rule` VALUES (209, 1, 'admin', 'admin_url', 'admin/music/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (210, 1, 'admin', 'admin_url', 'admin/music/canceldel', '', '取消删除', '');
INSERT INTO `cmf_auth_rule` VALUES (211, 1, 'admin', 'admin_url', 'admin/musiccat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (212, 1, 'admin', 'admin_url', 'admin/musiccat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (213, 1, 'admin', 'admin_url', 'admin/musiccat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (214, 1, 'admin', 'admin_url', 'admin/musiccat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (215, 1, 'admin', 'admin_url', 'admin/musiccat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (216, 1, 'admin', 'admin_url', 'admin/musiccat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (217, 1, 'admin', 'admin_url', 'admin/shop/default', '', '店铺管理', '');
INSERT INTO `cmf_auth_rule` VALUES (218, 1, 'admin', 'admin_url', 'admin/shopapply/index', '', '店铺申请', '');
INSERT INTO `cmf_auth_rule` VALUES (219, 1, 'admin', 'admin_url', 'admin/shopapply/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (220, 1, 'admin', 'admin_url', 'admin/shopapply/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (221, 1, 'admin', 'admin_url', 'admin/shopapply/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (222, 1, 'admin', 'admin_url', 'admin/shopbond/index', '', '保证金', '');
INSERT INTO `cmf_auth_rule` VALUES (223, 1, 'admin', 'admin_url', 'admin/shopbond/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (224, 1, 'admin', 'admin_url', 'admin/shopgoods/index', '', '商品管理', '');
INSERT INTO `cmf_auth_rule` VALUES (225, 1, 'admin', 'admin_url', 'admin/shopgoods/setstatus', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (226, 1, 'admin', 'admin_url', 'admin/shopgoods/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (227, 1, 'admin', 'admin_url', 'admin/turntable/default', '', '大转盘', '');
INSERT INTO `cmf_auth_rule` VALUES (228, 1, 'admin', 'admin_url', 'admin/turntablecon/index', '', '价格管理', '');
INSERT INTO `cmf_auth_rule` VALUES (229, 1, 'admin', 'admin_url', 'admin/turntablecon/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (230, 1, 'admin', 'admin_url', 'admin/turntablecon/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (231, 1, 'admin', 'admin_url', 'admin/turntablecon/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (232, 1, 'admin', 'admin_url', 'admin/turntable/index', '', '奖品管理', '');
INSERT INTO `cmf_auth_rule` VALUES (233, 1, 'admin', 'admin_url', 'admin/turntable/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (234, 1, 'admin', 'admin_url', 'admin/turntable/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (235, 1, 'admin', 'admin_url', 'admin/turntable/index2', '', '转盘记录', '');
INSERT INTO `cmf_auth_rule` VALUES (236, 1, 'admin', 'admin_url', 'admin/turntable/index3', '', '线下奖品', '');
INSERT INTO `cmf_auth_rule` VALUES (237, 1, 'admin', 'admin_url', 'admin/turntable/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (238, 1, 'admin', 'admin_url', 'admin/level/default', '', '等级管理', '');
INSERT INTO `cmf_auth_rule` VALUES (239, 1, 'admin', 'admin_url', 'admin/level/index', '', '用户等级', '');
INSERT INTO `cmf_auth_rule` VALUES (240, 1, 'admin', 'admin_url', 'admin/level/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (241, 1, 'admin', 'admin_url', 'admin/level/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (242, 1, 'admin', 'admin_url', 'admin/level/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (243, 1, 'admin', 'admin_url', 'admin/level/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (244, 1, 'admin', 'admin_url', 'admin/level/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (245, 1, 'admin', 'admin_url', 'admin/levelanchor/index', '', '主播等级', '');
INSERT INTO `cmf_auth_rule` VALUES (246, 1, 'admin', 'admin_url', 'admin/levelanchor/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (247, 1, 'admin', 'admin_url', 'admin/levelanchor/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (248, 1, 'admin', 'admin_url', 'admin/levelanchor/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (249, 1, 'admin', 'admin_url', 'admin/levelanchor/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (250, 1, 'admin', 'admin_url', 'admin/levelanchor/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (251, 1, 'admin', 'admin_url', 'admin/guard/index', '', '守护管理', '');
INSERT INTO `cmf_auth_rule` VALUES (252, 1, 'admin', 'admin_url', 'admin/guard/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (253, 1, 'admin', 'admin_url', 'admin/guard/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (254, 1, 'admin', 'admin_url', 'admin/liang/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (255, 1, 'admin', 'admin_url', 'admin/liang/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (256, 1, 'admin', 'admin_url', 'admin/liang/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (257, 1, 'admin', 'admin_url', 'admin/liang/setstatus', '', '设置状态', '');
INSERT INTO `cmf_auth_rule` VALUES (258, 1, 'admin', 'admin_url', 'admin/liang/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (259, 1, 'admin', 'admin_url', 'admin/vip/default', '', 'VIP管理', '');
INSERT INTO `cmf_auth_rule` VALUES (260, 1, 'admin', 'admin_url', 'admin/vip/index', '', 'VIP列表', '');
INSERT INTO `cmf_auth_rule` VALUES (261, 1, 'admin', 'admin_url', 'admin/vip/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (262, 1, 'admin', 'admin_url', 'admin/vip/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (263, 1, 'admin', 'admin_url', 'admin/vip/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (264, 1, 'admin', 'admin_url', 'admin/vipuser/index', '', 'VIP用户', '');
INSERT INTO `cmf_auth_rule` VALUES (265, 1, 'admin', 'admin_url', 'admin/vipuser/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (266, 1, 'admin', 'admin_url', 'admin/vipuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (267, 1, 'admin', 'admin_url', 'admin/vipuser/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (268, 1, 'admin', 'admin_url', 'admin/vipuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (269, 1, 'admin', 'admin_url', 'admin/vipuser/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (270, 1, 'admin', 'admin_url', 'admin/family/default', '', '家族管理', '');
INSERT INTO `cmf_auth_rule` VALUES (271, 1, 'admin', 'admin_url', 'admin/family/index', '', '家族列表', '');
INSERT INTO `cmf_auth_rule` VALUES (272, 1, 'admin', 'admin_url', 'admin/family/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (273, 1, 'admin', 'admin_url', 'admin/family/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (274, 1, 'admin', 'admin_url', 'admin/family/disable', '', '禁用', '');
INSERT INTO `cmf_auth_rule` VALUES (275, 1, 'admin', 'admin_url', 'admin/family/enable', '', '启用', '');
INSERT INTO `cmf_auth_rule` VALUES (276, 1, 'admin', 'admin_url', 'admin/family/profit', '', '收益', '');
INSERT INTO `cmf_auth_rule` VALUES (277, 1, 'admin', 'admin_url', 'admin/family/cash', '', '提现', '');
INSERT INTO `cmf_auth_rule` VALUES (278, 1, 'admin', 'admin_url', 'admin/family/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (279, 1, 'admin', 'admin_url', 'admin/familyuser/index', '', '成员管理', '');
INSERT INTO `cmf_auth_rule` VALUES (280, 1, 'admin', 'admin_url', 'admin/familyuser/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (281, 1, 'admin', 'admin_url', 'admin/familyuser/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (282, 1, 'admin', 'admin_url', 'admin/familyuser/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (283, 1, 'admin', 'admin_url', 'admin/familyuser/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (284, 1, 'admin', 'admin_url', 'admin/familyuser/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (285, 1, 'admin', 'admin_url', 'admin/finance/default', '', '财务管理', '');
INSERT INTO `cmf_auth_rule` VALUES (286, 1, 'admin', 'admin_url', 'admin/chargerules/index', '', '充值规则', '');
INSERT INTO `cmf_auth_rule` VALUES (287, 1, 'admin', 'admin_url', 'admin/chargerules/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (288, 1, 'admin', 'admin_url', 'admin/chargerules/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (289, 1, 'admin', 'admin_url', 'admin/chargerules/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (290, 1, 'admin', 'admin_url', 'admin/chargerules/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (291, 1, 'admin', 'admin_url', 'admin/chargerules/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (292, 1, 'admin', 'admin_url', 'admin/chargerules/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (293, 1, 'admin', 'admin_url', 'admin/charge/index', '', '钻石充值记录', '');
INSERT INTO `cmf_auth_rule` VALUES (294, 1, 'admin', 'admin_url', 'admin/charge/setpay', '', '确认支付', '');
INSERT INTO `cmf_auth_rule` VALUES (295, 1, 'admin', 'admin_url', 'admin/charge/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (296, 1, 'admin', 'admin_url', 'admin/manual/index', '', '手动充值钻石', '');
INSERT INTO `cmf_auth_rule` VALUES (297, 1, 'admin', 'admin_url', 'admin/manual/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (298, 1, 'admin', 'admin_url', 'admin/manual/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (299, 1, 'admin', 'admin_url', 'admin/manual/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (300, 1, 'admin', 'admin_url', 'admin/coinrecord/index', '', '钻石消费记录', '');
INSERT INTO `cmf_auth_rule` VALUES (301, 1, 'admin', 'admin_url', 'admin/cash/index', '', '映票提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (302, 1, 'admin', 'admin_url', 'admin/cash/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (303, 1, 'admin', 'admin_url', 'admin/cash/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (304, 1, 'admin', 'admin_url', 'admin/cash/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (305, 1, 'admin', 'admin_url', 'admin/guide/set', '', '引导页', '');
INSERT INTO `cmf_auth_rule` VALUES (306, 1, 'admin', 'admin_url', 'admin/guide/setPost', '', '设置提交', '');
INSERT INTO `cmf_auth_rule` VALUES (307, 1, 'admin', 'admin_url', 'admin/guide/index', '', '管理', '');
INSERT INTO `cmf_auth_rule` VALUES (308, 1, 'admin', 'admin_url', 'admin/guide/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (309, 1, 'admin', 'admin_url', 'admin/guide/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (310, 1, 'admin', 'admin_url', 'admin/guide/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (311, 1, 'admin', 'admin_url', 'admin/guide/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (312, 1, 'admin', 'admin_url', 'admin/guide/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (313, 1, 'admin', 'admin_url', 'admin/guide/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (314, 1, 'admin', 'admin_url', 'admin/auth/index', '', '身份认证', '');
INSERT INTO `cmf_auth_rule` VALUES (315, 1, 'admin', 'admin_url', 'admin/auth/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (316, 1, 'admin', 'admin_url', 'admin/auth/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (317, 1, 'admin', 'admin_url', 'admin/auth/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (318, 1, 'admin', 'admin_url', 'admin/video/index', 'isdel=0&status=1', '审核通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (319, 1, 'admin', 'admin_url', 'admin/video/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (320, 1, 'admin', 'admin_url', 'admin/video/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (321, 1, 'admin', 'admin_url', 'admin/video/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (322, 1, 'admin', 'admin_url', 'admin/video/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (323, 1, 'admin', 'admin_url', 'admin/video/setstatus', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (324, 1, 'admin', 'admin_url', 'admin/video/see', '', '查看', '');
INSERT INTO `cmf_auth_rule` VALUES (325, 1, 'admin', 'admin_url', 'admin/videocom/index', '', '视频评论', '');
INSERT INTO `cmf_auth_rule` VALUES (326, 1, 'admin', 'admin_url', 'admin/videocom/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (327, 1, 'admin', 'admin_url', 'admin/videorepcat/index', '', '举报类型', '');
INSERT INTO `cmf_auth_rule` VALUES (328, 1, 'admin', 'admin_url', 'admin/videorepcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (329, 1, 'admin', 'admin_url', 'admin/videorepcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (330, 1, 'admin', 'admin_url', 'admin/videorepcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (331, 1, 'admin', 'admin_url', 'admin/videorepcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (332, 1, 'admin', 'admin_url', 'admin/videorepcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (333, 1, 'admin', 'admin_url', 'admin/videorepcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (334, 1, 'admin', 'admin_url', 'admin/videorep/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (335, 1, 'admin', 'admin_url', 'admin/videorep/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (336, 1, 'admin', 'admin_url', 'admin/videorep/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (337, 1, 'admin', 'admin_url', 'admin/Loginbonus/index', '', '登录奖励', '');
INSERT INTO `cmf_auth_rule` VALUES (338, 1, 'admin', 'admin_url', 'admin/Loginbonus/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (339, 1, 'admin', 'admin_url', 'admin/Loginbonus/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (340, 1, 'admin', 'admin_url', 'admin/red/index', '', '红包管理', '');
INSERT INTO `cmf_auth_rule` VALUES (341, 1, 'admin', 'admin_url', 'admin/red/index2', '', '领取详情', '');
INSERT INTO `cmf_auth_rule` VALUES (342, 1, 'admin', 'admin_url', 'admin/note/default', '', '消息管理', '');
INSERT INTO `cmf_auth_rule` VALUES (343, 1, 'admin', 'admin_url', 'admin/sendcode/index', '', '验证码管理', '');
INSERT INTO `cmf_auth_rule` VALUES (344, 1, 'admin', 'admin_url', 'admin/sendcode/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (345, 1, 'admin', 'admin_url', 'admin/sendcode/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (346, 1, 'admin', 'admin_url', 'admin/push/index', '', '推送管理', '');
INSERT INTO `cmf_auth_rule` VALUES (347, 1, 'admin', 'admin_url', 'admin/push/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (348, 1, 'admin', 'admin_url', 'admin/push/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (349, 1, 'admin', 'admin_url', 'admin/push/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (350, 1, 'admin', 'admin_url', 'admin/system/index', '', '直播间消息', '');
INSERT INTO `cmf_auth_rule` VALUES (351, 1, 'admin', 'admin_url', 'admin/system/send', '', '发送', '');
INSERT INTO `cmf_auth_rule` VALUES (352, 1, 'admin', 'admin_url', 'admin/Impression/index', '', '标签管理', '');
INSERT INTO `cmf_auth_rule` VALUES (353, 1, 'admin', 'admin_url', 'admin/Impression/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (354, 1, 'admin', 'admin_url', 'admin/Impression/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (355, 1, 'admin', 'admin_url', 'admin/Impression/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (356, 1, 'admin', 'admin_url', 'admin/Impression/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (357, 1, 'admin', 'admin_url', 'admin/Impression/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (358, 1, 'admin', 'admin_url', 'admin/Impression/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (359, 1, 'admin', 'admin_url', 'admin/portal/default', '', '内容管理', '');
INSERT INTO `cmf_auth_rule` VALUES (360, 1, 'admin', 'admin_url', 'admin/feedback/index', '', '用户反馈', '');
INSERT INTO `cmf_auth_rule` VALUES (361, 1, 'admin', 'admin_url', 'admin/feedback/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (362, 1, 'admin', 'admin_url', 'admin/feedback/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (363, 1, 'portal', 'admin_url', 'portal/AdminPage/index', '', '页面管理', '');
INSERT INTO `cmf_auth_rule` VALUES (364, 1, 'portal', 'admin_url', 'portal/AdminPage/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (365, 1, 'portal', 'admin_url', 'portal/AdminPage/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (366, 1, 'portal', 'admin_url', 'portal/AdminPage/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (367, 1, 'portal', 'admin_url', 'portal/AdminPage/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (368, 1, 'portal', 'admin_url', 'portal/AdminPage/delete', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (369, 1, 'admin', 'admin_url', 'admin/videoclass/index', '', '视频分类', '');
INSERT INTO `cmf_auth_rule` VALUES (370, 1, 'admin', 'admin_url', 'admin/videoclass/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (371, 1, 'admin', 'admin_url', 'admin/videoclass/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (372, 1, 'admin', 'admin_url', 'admin/videoclass/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (373, 1, 'admin', 'admin_url', 'admin/videoclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (374, 1, 'admin', 'admin_url', 'admin/videoclass/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (375, 1, 'admin', 'admin_url', 'admin/videoclass/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (376, 1, 'admin', 'admin_url', 'admin/sticker/index', '', '贴纸列表', '');
INSERT INTO `cmf_auth_rule` VALUES (377, 1, 'admin', 'admin_url', 'admin/sticker/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (378, 1, 'admin', 'admin_url', 'admin/sticker/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (379, 1, 'admin', 'admin_url', 'admin/sticker/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (380, 1, 'admin', 'admin_url', 'admin/sticker/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (381, 1, 'admin', 'admin_url', 'admin/sticker/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (382, 1, 'admin', 'admin_url', 'admin/sticker/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (383, 1, 'admin', 'admin_url', 'admin/Dynamic/default', '', '动态管理', '');
INSERT INTO `cmf_auth_rule` VALUES (384, 1, 'admin', 'admin_url', 'admin/Dynamic/index', 'isdel=0&status=1', '审核通过', '');
INSERT INTO `cmf_auth_rule` VALUES (385, 1, 'admin', 'admin_url', 'admin/Dynamic/wait', 'isdel=0&status=0', '等待审核', '');
INSERT INTO `cmf_auth_rule` VALUES (386, 1, 'admin', 'admin_url', 'admin/Dynamic/nopass', 'isdel=0&status=-1', '未通过', '');
INSERT INTO `cmf_auth_rule` VALUES (387, 1, 'admin', 'admin_url', 'admin/Dynamic/lower', 'isdel=1', '下架列表', '');
INSERT INTO `cmf_auth_rule` VALUES (388, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/index', '', '举报类型', '');
INSERT INTO `cmf_auth_rule` VALUES (389, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (390, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (391, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (392, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (393, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (394, 1, 'admin', 'admin_url', 'admin/Dynamicrepotcat/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (395, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/index', '', '举报列表', '');
INSERT INTO `cmf_auth_rule` VALUES (396, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/setstatus', '', '处理', '');
INSERT INTO `cmf_auth_rule` VALUES (397, 1, 'admin', 'admin_url', 'admin/Dynamicrepot/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (398, 1, 'admin', 'admin_url', 'admin/Dynamiccom/index', '', '评论列表', '');
INSERT INTO `cmf_auth_rule` VALUES (399, 1, 'admin', 'admin_url', 'admin/Dynamiccom/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (400, 1, 'admin', 'admin_url', 'admin/video/wait', 'isdel=0&status=0&is_draft=0', '待审核列表', '');
INSERT INTO `cmf_auth_rule` VALUES (401, 1, 'admin', 'admin_url', 'admin/video/nopass', 'isdel=0&status=2', '未通过列表', '');
INSERT INTO `cmf_auth_rule` VALUES (402, 1, 'admin', 'admin_url', 'admin/video/lower', 'isdel=1', '下架列表', '');
INSERT INTO `cmf_auth_rule` VALUES (403, 1, 'admin', 'admin_url', 'admin/dynamic/setstatus', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (404, 1, 'admin', 'admin_url', 'admin/dynamic/see', '', '查看', '');
INSERT INTO `cmf_auth_rule` VALUES (405, 1, 'admin', 'admin_url', 'admin/dynamic/setdel', '', '上下架', '');
INSERT INTO `cmf_auth_rule` VALUES (406, 1, 'admin', 'admin_url', 'admin/dynamic/setrecom', '', '设置推荐值', '');
INSERT INTO `cmf_auth_rule` VALUES (407, 1, 'admin', 'admin_url', 'admin/Dynamic/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (408, 1, 'admin', 'admin_url', 'admin/game/index', '', '游戏记录', '');
INSERT INTO `cmf_auth_rule` VALUES (409, 1, 'admin', 'admin_url', 'admin/game/index2', '', 'x详情', '');
INSERT INTO `cmf_auth_rule` VALUES (410, 1, 'user', 'admin_url', 'user/AdminIndex/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (411, 1, 'user', 'admin_url', 'user/AdminIndex/addPost', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (412, 1, 'user', 'admin_url', 'user/AdminIndex/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (413, 1, 'user', 'admin_url', 'user/AdminIndex/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (414, 1, 'user', 'admin_url', 'user/AdminIndex/setban', '', '禁用时间', '');
INSERT INTO `cmf_auth_rule` VALUES (415, 1, 'user', 'admin_url', 'user/AdminIndex/setsuper', '', '设置、取消超管', '');
INSERT INTO `cmf_auth_rule` VALUES (416, 1, 'user', 'admin_url', 'user/AdminIndex/sethot', '', '设置取消热门', '');
INSERT INTO `cmf_auth_rule` VALUES (417, 1, 'user', 'admin_url', 'user/AdminIndex/setrecommend', '', '设置取消推荐', '');
INSERT INTO `cmf_auth_rule` VALUES (418, 1, 'user', 'admin_url', 'user/AdminIndex/setzombie', '', '开启关闭僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (419, 1, 'user', 'admin_url', 'user/AdminIndex/setzombiep', '', '设置取消为僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (420, 1, 'user', 'admin_url', 'user/adminIndex/setzombiepall', '', '批量设置/取消为僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (421, 1, 'user', 'admin_url', 'user/adminIndex/setzombieall', '', '一键开启/关闭僵尸粉', '');
INSERT INTO `cmf_auth_rule` VALUES (422, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/index', '', '商品分类列表', '');
INSERT INTO `cmf_auth_rule` VALUES (423, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/add', '', '商品分类添加', '');
INSERT INTO `cmf_auth_rule` VALUES (424, 1, 'Admin', 'admin_url', 'Admin/Buyeraddress/index', '', '收货地址管理', '');
INSERT INTO `cmf_auth_rule` VALUES (425, 1, 'Admin', 'admin_url', 'Admin/Express/index', '', '物流公司列表', '');
INSERT INTO `cmf_auth_rule` VALUES (426, 1, 'Admin', 'admin_url', 'Admin/Express/add', '', '物流公司添加', '');
INSERT INTO `cmf_auth_rule` VALUES (427, 1, 'Admin', 'admin_url', 'Admin/Express/add_post', '', '物流公司添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (428, 1, 'Admin', 'admin_url', 'Admin/Express/edit', '', '物流公司编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (429, 1, 'Admin', 'admin_url', 'Admin/Express/edit_post', '', '物流公司编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (430, 1, 'Admin', 'admin_url', 'Admin/Express/del', '', '物流公司删除', '');
INSERT INTO `cmf_auth_rule` VALUES (431, 1, 'Admin', 'admin_url', 'Admin/Express/listOrder', '', '物流公司排序', '');
INSERT INTO `cmf_auth_rule` VALUES (432, 1, 'Admin', 'admin_url', 'Admin/Express/changeStatus', '', '物流公司显示/隐藏', '');
INSERT INTO `cmf_auth_rule` VALUES (433, 1, 'Admin', 'admin_url', 'Admin/Refundreason/index', '', '买家申请退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (434, 1, 'Admin', 'admin_url', 'Admin/Refundreason/add', '', '添加退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (435, 1, 'Admin', 'admin_url', 'Admin/Refundreason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (436, 1, 'Admin', 'admin_url', 'Admin/Refundreason/edit', '', '编辑退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (437, 1, 'Admin', 'admin_url', 'Admin/Refundreason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (438, 1, 'Admin', 'admin_url', 'Admin/Refundreason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (439, 1, 'Admin', 'admin_url', 'Admin/Refundreason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (440, 1, 'Admin', 'admin_url', 'Admin/Refusereason/index', '', '卖家拒绝退款原因', '');
INSERT INTO `cmf_auth_rule` VALUES (441, 1, 'Admin', 'admin_url', 'Admin/Platformreason/index', '', '退款平台介入原因', '');
INSERT INTO `cmf_auth_rule` VALUES (442, 1, 'Admin', 'admin_url', 'Admin/Goodsorder/index', '', '商品订单列表', '');
INSERT INTO `cmf_auth_rule` VALUES (443, 1, 'Admin', 'admin_url', 'Admin/Refundlist/index', '', '退款列表', '');
INSERT INTO `cmf_auth_rule` VALUES (444, 1, 'Admin', 'admin_url', 'Admin/Shopcash/index', '', '提现记录', '');
INSERT INTO `cmf_auth_rule` VALUES (445, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/default', '', '付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (446, 1, 'Admin', 'admin_url', 'Admin/Refusereason/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (447, 1, 'Admin', 'admin_url', 'Admin/Refusereason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (448, 1, 'Admin', 'admin_url', 'Admin/Refusereason/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (449, 1, 'Admin', 'admin_url', 'Admin/Refusereason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (450, 1, 'Admin', 'admin_url', 'Admin/Refusereason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (451, 1, 'Admin', 'admin_url', 'Admin/Refusereason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (452, 1, 'Admin', 'admin_url', 'Admin/Platformreason/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (453, 1, 'Admin', 'admin_url', 'Admin/Platformreason/add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (454, 1, 'Admin', 'admin_url', 'Admin/Platformreason/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (455, 1, 'Admin', 'admin_url', 'Admin/Platformreason/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (456, 1, 'Admin', 'admin_url', 'Admin/Platformreason/listOrder', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (457, 1, 'Admin', 'admin_url', 'Admin/Platformreason/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (458, 1, 'Admin', 'admin_url', 'Admin/Goodsorder/info', '', '订单详情', '');
INSERT INTO `cmf_auth_rule` VALUES (459, 1, 'Admin', 'admin_url', 'Admin/Refundlist/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (460, 1, 'Admin', 'admin_url', 'Admin/Refundlist/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (461, 1, 'Admin', 'admin_url', 'Admin/Shopcash/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (462, 1, 'Admin', 'admin_url', 'Admin/Shopcash/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (463, 1, 'Admin', 'admin_url', 'Admin/Shopcash/export', '', '导出', '');
INSERT INTO `cmf_auth_rule` VALUES (464, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/classlist', '', '付费内容分类列表', '');
INSERT INTO `cmf_auth_rule` VALUES (465, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_add', '', '添加付费内容分类', '');
INSERT INTO `cmf_auth_rule` VALUES (466, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_add_post', '', '添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (467, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_edit', '', '编辑付费内容分类', '');
INSERT INTO `cmf_auth_rule` VALUES (468, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (469, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/class_del', '', '付费内容分类删除', '');
INSERT INTO `cmf_auth_rule` VALUES (470, 1, 'Admin', 'admin_url', 'Admin/Paidprogramclass/listOrder', '', '付费内容分类排序', '');
INSERT INTO `cmf_auth_rule` VALUES (471, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/index', '', '付费内容列表', '');
INSERT INTO `cmf_auth_rule` VALUES (472, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/applylist', '', '付费内容申请列表', '');
INSERT INTO `cmf_auth_rule` VALUES (473, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/orderlist', '', '付费内容订单', '');
INSERT INTO `cmf_auth_rule` VALUES (474, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/addPost', '', '商品分类添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (475, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/edit', '', '商品审核/详情', '');
INSERT INTO `cmf_auth_rule` VALUES (476, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/editPost', '', '商品审核提交', '');
INSERT INTO `cmf_auth_rule` VALUES (477, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/edit', '', '商品分类编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (478, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (479, 1, 'Admin', 'admin_url', 'Admin/Goodsclass/del', '', '商品分类删除', '');
INSERT INTO `cmf_auth_rule` VALUES (480, 1, 'Admin', 'admin_url', 'Admin/Buyeraddress/del', '', '收货地址删除', '');
INSERT INTO `cmf_auth_rule` VALUES (481, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/commentlist', '', '评价列表', '');
INSERT INTO `cmf_auth_rule` VALUES (482, 1, 'Admin', 'admin_url', 'Admin/Shopgoods/delComment', '', '删除评价', '');
INSERT INTO `cmf_auth_rule` VALUES (483, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/edit', '', '编辑付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (484, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (485, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/del', '', '删除付费内容', '');
INSERT INTO `cmf_auth_rule` VALUES (486, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/videoplay', '', '付费内容观看', '');
INSERT INTO `cmf_auth_rule` VALUES (487, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_edit', '', '编辑付费内容申请', '');
INSERT INTO `cmf_auth_rule` VALUES (488, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_edit_post', '', '付费内容申请编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (489, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/apply_del', '', '删除付费内容申请', '');
INSERT INTO `cmf_auth_rule` VALUES (490, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/setPay', '', '确认支付', '');
INSERT INTO `cmf_auth_rule` VALUES (491, 1, 'Admin', 'admin_url', 'Admin/Paidprogram/export', '', '导出付费内容订单', '');
INSERT INTO `cmf_auth_rule` VALUES (492, 1, 'Admin', 'admin_url', 'Admin/Video/draft', 'is_draft=1', '草稿视频列表', '');
INSERT INTO `cmf_auth_rule` VALUES (493, 1, 'Admin', 'admin_url', 'Admin/Balance/index', '', '余额手动充值', '');
INSERT INTO `cmf_auth_rule` VALUES (494, 1, 'Admin', 'admin_url', 'Admin/Balance/add', '', '充值添加', '');
INSERT INTO `cmf_auth_rule` VALUES (495, 1, 'Admin', 'admin_url', 'Admin/Balance/addPost', '', '余额充值保存', '');
INSERT INTO `cmf_auth_rule` VALUES (496, 1, 'Admin', 'admin_url', 'Admin/Balance/export', '', '余额充值记录导出', '');
INSERT INTO `cmf_auth_rule` VALUES (497, 1, 'admin', 'admin_url', 'admin/goodsgroup/index', '', '拼团列表', '');
INSERT INTO `cmf_auth_rule` VALUES (498, 1, 'admin', 'admin_url', 'admin/familyuser/divideapply', '', '分成申请列表', '');
INSERT INTO `cmf_auth_rule` VALUES (499, 1, 'admin', 'admin_url', 'admin/familyuser/applyedit', '', '审核', '');
INSERT INTO `cmf_auth_rule` VALUES (500, 1, 'admin', 'admin_url', 'admin/familyuser/applyeditPost', '', '审核提交', '');
INSERT INTO `cmf_auth_rule` VALUES (501, 1, 'admin', 'admin_url', 'admin/familyuser/delapply', '', '删除审核', '');
INSERT INTO `cmf_auth_rule` VALUES (502, 1, 'admin', 'admin_url', 'admin/Scorerecord/index', '', '积分记录', '');
INSERT INTO `cmf_auth_rule` VALUES (503, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/index', '', '话题标签', '');
INSERT INTO `cmf_auth_rule` VALUES (504, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/add', '', '添加', '');
INSERT INTO `cmf_auth_rule` VALUES (505, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/add_post', '', '添加提交', '');
INSERT INTO `cmf_auth_rule` VALUES (506, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (507, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/edit_post', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (508, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/listsorders', '', '排序', '');
INSERT INTO `cmf_auth_rule` VALUES (509, 1, 'admin', 'admin_url', 'admin/Dynamiclabel/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (510, 1, 'user', 'admin_url', 'user/AdminIndex/del', '', '本站用户删除', '');
INSERT INTO `cmf_auth_rule` VALUES (511, 1, 'Admin', 'admin_url', 'Admin/Shopcategory/index', '', '经营类目申请', '');
INSERT INTO `cmf_auth_rule` VALUES (512, 1, 'admin', 'admin_url', 'admin/Shopcategory/edit', '', '编辑', '');
INSERT INTO `cmf_auth_rule` VALUES (513, 1, 'admin', 'admin_url', 'admin/Shopcategory/editPost', '', '编辑提交', '');
INSERT INTO `cmf_auth_rule` VALUES (514, 1, 'admin', 'admin_url', 'admin/Shopcategory/del', '', '删除', '');
INSERT INTO `cmf_auth_rule` VALUES (515, 1, 'admin', 'admin_url', 'admin/shopapply/platformedit', '', '平台自营店铺信息', '');
INSERT INTO `cmf_auth_rule` VALUES (516, 1, 'admin', 'admin_url', 'admin/shopapply/platformeditPost', '', '平台自营店铺提交', '');
INSERT INTO `cmf_auth_rule` VALUES (517, 1, 'admin', 'admin_url', 'admin/shopgoods/add', '', '添加商品', '');
INSERT INTO `cmf_auth_rule` VALUES (518, 1, 'admin', 'admin_url', 'admin/shopgoods/addpost', '', '商品添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (519, 1, 'admin', 'admin_url', 'admin/goodsorder/setexpress', '', '填写物流', '');
INSERT INTO `cmf_auth_rule` VALUES (520, 1, 'admin', 'admin_url', 'admin/goodsorder/setexpresspost', '', '填写物流提交', '');
INSERT INTO `cmf_auth_rule` VALUES (521, 1, 'admin', 'admin_url', 'admin/refundlist/platformedit', '', '平台自营处理退款', '');
INSERT INTO `cmf_auth_rule` VALUES (522, 1, 'admin', 'admin_url', 'admin/refundlist/platformedit_post', '', '平台自营商品退款提交', '');
INSERT INTO `cmf_auth_rule` VALUES (523, 1, 'Admin', 'admin_url', 'Admin/Advert/index', '', '广告视频列表', '');
INSERT INTO `cmf_auth_rule` VALUES (524, 1, 'Admin', 'admin_url', 'Admin/Advert/add', '', '添加广告视频', '');
INSERT INTO `cmf_auth_rule` VALUES (525, 1, 'Admin', 'admin_url', 'Admin/Advert/add_post', '', '广告视频添加保存', '');
INSERT INTO `cmf_auth_rule` VALUES (526, 1, 'Admin', 'admin_url', 'Admin/Advert/lowervideo', '', '下架广告视频列表', '');
INSERT INTO `cmf_auth_rule` VALUES (527, 1, 'Admin', 'admin_url', 'Admin/Advert/edit', '', '编辑广告视频', '');
INSERT INTO `cmf_auth_rule` VALUES (528, 1, 'Admin', 'admin_url', 'Admin/Advert/edit_post', '', '广告视频编辑保存', '');
INSERT INTO `cmf_auth_rule` VALUES (529, 1, 'Admin', 'admin_url', 'Admin/Advert/setXiajia', '', '广告视频下架', '');
INSERT INTO `cmf_auth_rule` VALUES (530, 1, 'Admin', 'admin_url', 'Admin/Advert/video_listen', '', '观看广告视频', '');
INSERT INTO `cmf_auth_rule` VALUES (531, 1, 'Admin', 'admin_url', 'Admin/Advert/set_shangjia', '', '上架广告视频', '');
INSERT INTO `cmf_auth_rule` VALUES (532, 1, 'Admin', 'admin_url', 'Admin/Advert/listorders', '', '广告视频排序', '');
INSERT INTO `cmf_auth_rule` VALUES (533, 1, 'Admin', 'admin_url', 'Admin/Advert/commentlists', '', '广告视频评论列表', '');
INSERT INTO `cmf_auth_rule` VALUES (534, 1, 'Admin', 'admin_url', 'Admin/Advert/del', '', '删除广告视频', '');
INSERT INTO `cmf_auth_rule` VALUES (535, 1, 'Admin', 'admin_url', 'Admin/Advert/firstcharge', '', '首充规则', '');

-- ----------------------------
-- Table structure for cmf_backpack
-- ----------------------------
DROP TABLE IF EXISTS `cmf_backpack`;
CREATE TABLE `cmf_backpack`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `giftid` int(11) NOT NULL DEFAULT 0 COMMENT '礼物ID',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_cash_account
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_account`;
CREATE TABLE `cmf_cash_account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型，1表示支付宝，2表示微信，3表示银行卡',
  `account_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id_uid`(`id`, `uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_cash_account
-- ----------------------------
INSERT INTO `cmf_cash_account` VALUES (1, 7, 1, '', '周文宇', '15311111111', 1645153184);
INSERT INTO `cmf_cash_account` VALUES (2, 9, 1, '', '这玩意', '15314090000', 1645153975);

-- ----------------------------
-- Table structure for cmf_cash_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_cash_record`;
CREATE TABLE `cmf_cash_record`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `money` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '提现金额',
  `votes` int(20) NOT NULL DEFAULT 0 COMMENT '提现映票数',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方订单号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态，0审核中，1审核通过，2审核拒绝',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '申请时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '账号类型',
  `account_bank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '银行名称',
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '帐号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_cash_record
-- ----------------------------
INSERT INTO `cmf_cash_record` VALUES (1, 9, 9.50, 1000, '9_1645153977132', '', 1, 1645153977, 1645154000, 1, '', '15314090000', '这玩意');

-- ----------------------------
-- Table structure for cmf_charge_admin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_admin`;
CREATE TABLE `cmf_charge_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '充值对象ID',
  `coin` int(20) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_charge_admin
-- ----------------------------
INSERT INTO `cmf_charge_admin` VALUES (1, 4, 999999, 1644978275, 'admin', '660052928');
INSERT INTO `cmf_charge_admin` VALUES (2, 3, 999999, 1644982108, 'admin', '659909365');
INSERT INTO `cmf_charge_admin` VALUES (3, 9, 99999, 1645152906, 'admin', '1895433577');
INSERT INTO `cmf_charge_admin` VALUES (4, 7, 1000000, 1645153762, 'admin', '1895433577');
INSERT INTO `cmf_charge_admin` VALUES (5, 8, 9999999, 1645251165, 'admin', '1895433577');

-- ----------------------------
-- Table structure for cmf_charge_rules
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_rules`;
CREATE TABLE `cmf_charge_rules`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '安卓金额',
  `give` int(11) NOT NULL DEFAULT 0 COMMENT '赠送钻石数',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_charge_rules
-- ----------------------------
INSERT INTO `cmf_charge_rules` VALUES (1, '1000钻石', 99999, 0.01, 0, 0, 1484984685);
INSERT INTO `cmf_charge_rules` VALUES (2, '3000钻石', 3000, 30.00, 0, 1, 1484985389);
INSERT INTO `cmf_charge_rules` VALUES (3, '9800钻石', 9800, 98.00, 200, 2, 1484985412);
INSERT INTO `cmf_charge_rules` VALUES (4, '38800钻石', 38800, 388.00, 500, 3, 1484985445);
INSERT INTO `cmf_charge_rules` VALUES (5, '58800钻石', 58800, 588.00, 1200, 4, 1484985458);

-- ----------------------------
-- Table structure for cmf_charge_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_charge_user`;
CREATE TABLE `cmf_charge_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(11) NOT NULL DEFAULT 0 COMMENT '充值对象ID',
  `money` decimal(11, 2) NOT NULL DEFAULT 0.00 COMMENT '金额',
  `coin` int(11) NOT NULL DEFAULT 0 COMMENT '钻石数',
  `coin_give` int(11) NOT NULL DEFAULT 0 COMMENT '赠送钻石数',
  `orderno` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '商家订单号',
  `trade_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方平台订单号',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '支付类型 1支付宝 2 微信支付 3 苹果支付 4 微信小程序 5 paypal 6 braintree_paypal',
  `ambient` tinyint(1) NOT NULL DEFAULT 0 COMMENT '支付环境',
  `is_first` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是首充规则 0 否 1 是',
  `score` int(11) NOT NULL DEFAULT 0 COMMENT '首充赠送积分',
  `vip_length` int(11) NOT NULL DEFAULT 0 COMMENT '首充赠送vip时长',
  `giftid` int(11) NOT NULL DEFAULT 0 COMMENT '首充赠送礼物id',
  `gift_num` int(11) NOT NULL DEFAULT 0 COMMENT '首充赠送礼物数量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_getcode_limit_ip
-- ----------------------------
DROP TABLE IF EXISTS `cmf_getcode_limit_ip`;
CREATE TABLE `cmf_getcode_limit_ip`  (
  `ip` bigint(20) NOT NULL COMMENT 'ip地址',
  `date` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '时间',
  `times` tinyint(4) NOT NULL DEFAULT 0 COMMENT '次数',
  PRIMARY KEY (`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_gift
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift`;
CREATE TABLE `cmf_gift`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mark` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识，0普通，1热门',
  `type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '类型,0普通礼物，1豪华礼物',
  `sid` int(11) NOT NULL DEFAULT 0 COMMENT '分类ID',
  `giftname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `needcoin` int(11) NOT NULL DEFAULT 0 COMMENT '价格',
  `gifticon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片',
  `list_order` int(3) NOT NULL DEFAULT 9999 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `swftype` tinyint(1) NOT NULL DEFAULT 0 COMMENT '动画类型，0gif,1svga',
  `swf` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '动画链接',
  `swftime` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT '动画时长',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 123 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_gift
-- ----------------------------
INSERT INTO `cmf_gift` VALUES (7, 0, 1, 0, '跑车', 300, 'gift_7.png', 9999, 1578386860, 1, 'gift_gif_7.svga', 3.50);
INSERT INTO `cmf_gift` VALUES (14, 0, 0, 0, '冰淇淋', 50, 'gift_14.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (15, 0, 0, 0, '粉丝牌', 100, 'gift_15.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (16, 0, 0, 0, '干杯', 10, 'gift_16.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (18, 0, 0, 0, '金话筒', 130, 'gift_18.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (21, 1, 0, 0, '吃药啦', 1, 'gift_21.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (22, 0, 0, 0, '666', 2, 'gift_22.png', 9999, 1578386860, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (77, 0, 1, 0, '星际火箭', 1000, 'gift_31.png', 3, 1578386860, 1, 'gift_gif_31.svga', 3.70);
INSERT INTO `cmf_gift` VALUES (120, 0, 0, 0, '甜甜圈', 20, 'qiniu_admin/20220219/1deb65fcb7ecca0e23d5cdef68ea50e2.png', 9999, 1645238651, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (121, 0, 0, 0, '红包', 50, 'qiniu_admin/20220219/067ebd4642bb0875541ef5cadef59e62.png', 9999, 1645238665, 0, '', 0.00);
INSERT INTO `cmf_gift` VALUES (122, 0, 0, 0, '红唇', 10, 'qiniu_admin/20220219/67d6e5cd3a77258d7c8b5734ca7da370.png', 9999, 1645238694, 0, '', 0.00);

-- ----------------------------
-- Table structure for cmf_gift_sort
-- ----------------------------
DROP TABLE IF EXISTS `cmf_gift_sort`;
CREATE TABLE `cmf_gift_sort`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `sortname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `orderno` int(3) NOT NULL DEFAULT 0 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_guide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guide`;
CREATE TABLE `cmf_guide`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '图片/视频链接',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '跳转链接',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '类型',
  `list_order` int(11) NOT NULL DEFAULT 10000 COMMENT '序号',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `uptime` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_guide
-- ----------------------------
INSERT INTO `cmf_guide` VALUES (1, 'admin/20220219/649fc373b079abde037f811a3719d1e5.jpg', '', 0, 10000, 1645241787, 1645241787);

-- ----------------------------
-- Table structure for cmf_hook
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook`;
CREATE TABLE `cmf_hook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '钩子类型(1:系统钩子;2:应用钩子;3:模板钩子;4:后台模板钩子)',
  `once` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否只允许一个插件运行(0:多个;1:一个)',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子名称',
  `hook` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子',
  `app` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '应用名(只有应用钩子才用)',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统钩子表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_hook
-- ----------------------------
INSERT INTO `cmf_hook` VALUES (2, 1, 0, '应用开始', 'app_begin', 'cmf', '应用开始');
INSERT INTO `cmf_hook` VALUES (3, 1, 0, '模块初始化', 'module_init', 'cmf', '模块初始化');
INSERT INTO `cmf_hook` VALUES (4, 1, 0, '控制器开始', 'action_begin', 'cmf', '控制器开始');
INSERT INTO `cmf_hook` VALUES (5, 1, 0, '视图输出过滤', 'view_filter', 'cmf', '视图输出过滤');
INSERT INTO `cmf_hook` VALUES (6, 1, 0, '应用结束', 'app_end', 'cmf', '应用结束');
INSERT INTO `cmf_hook` VALUES (7, 1, 0, '日志write方法', 'log_write', 'cmf', '日志write方法');
INSERT INTO `cmf_hook` VALUES (8, 1, 0, '输出结束', 'response_end', 'cmf', '输出结束');
INSERT INTO `cmf_hook` VALUES (9, 1, 0, '后台控制器初始化', 'admin_init', 'cmf', '后台控制器初始化');
INSERT INTO `cmf_hook` VALUES (10, 1, 0, '前台控制器初始化', 'home_init', 'cmf', '前台控制器初始化');
INSERT INTO `cmf_hook` VALUES (11, 1, 1, '发送手机验证码', 'send_mobile_verification_code', 'cmf', '发送手机验证码');
INSERT INTO `cmf_hook` VALUES (12, 3, 0, '模板 body标签开始', 'body_start', '', '模板 body标签开始');
INSERT INTO `cmf_hook` VALUES (13, 3, 0, '模板 head标签结束前', 'before_head_end', '', '模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (14, 3, 0, '模板底部开始', 'footer_start', '', '模板底部开始');
INSERT INTO `cmf_hook` VALUES (15, 3, 0, '模板底部开始之前', 'before_footer', '', '模板底部开始之前');
INSERT INTO `cmf_hook` VALUES (16, 3, 0, '模板底部结束之前', 'before_footer_end', '', '模板底部结束之前');
INSERT INTO `cmf_hook` VALUES (17, 3, 0, '模板 body 标签结束之前', 'before_body_end', '', '模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (18, 3, 0, '模板左边栏开始', 'left_sidebar_start', '', '模板左边栏开始');
INSERT INTO `cmf_hook` VALUES (19, 3, 0, '模板左边栏结束之前', 'before_left_sidebar_end', '', '模板左边栏结束之前');
INSERT INTO `cmf_hook` VALUES (20, 3, 0, '模板右边栏开始', 'right_sidebar_start', '', '模板右边栏开始');
INSERT INTO `cmf_hook` VALUES (21, 3, 0, '模板右边栏结束之前', 'before_right_sidebar_end', '', '模板右边栏结束之前');
INSERT INTO `cmf_hook` VALUES (22, 3, 1, '评论区', 'comment', '', '评论区');
INSERT INTO `cmf_hook` VALUES (23, 3, 1, '留言区', 'guestbook', '', '留言区');
INSERT INTO `cmf_hook` VALUES (24, 2, 0, '后台首页仪表盘', 'admin_dashboard', 'admin', '后台首页仪表盘');
INSERT INTO `cmf_hook` VALUES (25, 4, 0, '后台模板 head标签结束前', 'admin_before_head_end', '', '后台模板 head标签结束前');
INSERT INTO `cmf_hook` VALUES (26, 4, 0, '后台模板 body 标签结束之前', 'admin_before_body_end', '', '后台模板 body 标签结束之前');
INSERT INTO `cmf_hook` VALUES (27, 2, 0, '后台登录页面', 'admin_login', 'admin', '后台登录页面');
INSERT INTO `cmf_hook` VALUES (28, 1, 1, '前台模板切换', 'switch_theme', 'cmf', '前台模板切换');
INSERT INTO `cmf_hook` VALUES (29, 3, 0, '主要内容之后', 'after_content', '', '主要内容之后');
INSERT INTO `cmf_hook` VALUES (32, 2, 1, '获取上传界面', 'fetch_upload_view', 'user', '获取上传界面');
INSERT INTO `cmf_hook` VALUES (33, 3, 0, '主要内容之前', 'before_content', 'cmf', '主要内容之前');
INSERT INTO `cmf_hook` VALUES (34, 1, 0, '日志写入完成', 'log_write_done', 'cmf', '日志写入完成');
INSERT INTO `cmf_hook` VALUES (35, 1, 1, '后台模板切换', 'switch_admin_theme', 'cmf', '后台模板切换');
INSERT INTO `cmf_hook` VALUES (36, 1, 1, '验证码图片', 'captcha_image', 'cmf', '验证码图片');
INSERT INTO `cmf_hook` VALUES (37, 2, 1, '后台模板设计界面', 'admin_theme_design_view', 'admin', '后台模板设计界面');
INSERT INTO `cmf_hook` VALUES (38, 2, 1, '后台设置网站信息界面', 'admin_setting_site_view', 'admin', '后台设置网站信息界面');
INSERT INTO `cmf_hook` VALUES (39, 2, 1, '后台清除缓存界面', 'admin_setting_clear_cache_view', 'admin', '后台清除缓存界面');
INSERT INTO `cmf_hook` VALUES (40, 2, 1, '后台导航管理界面', 'admin_nav_index_view', 'admin', '后台导航管理界面');
INSERT INTO `cmf_hook` VALUES (41, 2, 1, '后台友情链接管理界面', 'admin_link_index_view', 'admin', '后台友情链接管理界面');
INSERT INTO `cmf_hook` VALUES (42, 2, 1, '后台幻灯片管理界面', 'admin_slide_index_view', 'admin', '后台幻灯片管理界面');
INSERT INTO `cmf_hook` VALUES (43, 2, 1, '后台管理员列表界面', 'admin_user_index_view', 'admin', '后台管理员列表界面');
INSERT INTO `cmf_hook` VALUES (44, 2, 1, '后台角色管理界面', 'admin_rbac_index_view', 'admin', '后台角色管理界面');
INSERT INTO `cmf_hook` VALUES (49, 2, 1, '用户管理本站用户列表界面', 'user_admin_index_view', 'user', '用户管理本站用户列表界面');
INSERT INTO `cmf_hook` VALUES (50, 2, 1, '资源管理列表界面', 'user_admin_asset_index_view', 'user', '资源管理列表界面');
INSERT INTO `cmf_hook` VALUES (51, 2, 1, '用户管理第三方用户列表界面', 'user_admin_oauth_index_view', 'user', '用户管理第三方用户列表界面');
INSERT INTO `cmf_hook` VALUES (52, 2, 1, '后台首页界面', 'admin_index_index_view', 'admin', '后台首页界面');
INSERT INTO `cmf_hook` VALUES (53, 2, 1, '后台回收站界面', 'admin_recycle_bin_index_view', 'admin', '后台回收站界面');
INSERT INTO `cmf_hook` VALUES (54, 2, 1, '后台菜单管理界面', 'admin_menu_index_view', 'admin', '后台菜单管理界面');
INSERT INTO `cmf_hook` VALUES (55, 2, 1, '后台自定义登录是否开启钩子', 'admin_custom_login_open', 'admin', '后台自定义登录是否开启钩子');
INSERT INTO `cmf_hook` VALUES (64, 2, 1, '后台幻灯片页面列表界面', 'admin_slide_item_index_view', 'admin', '后台幻灯片页面列表界面');
INSERT INTO `cmf_hook` VALUES (65, 2, 1, '后台幻灯片页面添加界面', 'admin_slide_item_add_view', 'admin', '后台幻灯片页面添加界面');
INSERT INTO `cmf_hook` VALUES (66, 2, 1, '后台幻灯片页面编辑界面', 'admin_slide_item_edit_view', 'admin', '后台幻灯片页面编辑界面');
INSERT INTO `cmf_hook` VALUES (67, 2, 1, '后台管理员添加界面', 'admin_user_add_view', 'admin', '后台管理员添加界面');
INSERT INTO `cmf_hook` VALUES (68, 2, 1, '后台管理员编辑界面', 'admin_user_edit_view', 'admin', '后台管理员编辑界面');
INSERT INTO `cmf_hook` VALUES (69, 2, 1, '后台角色添加界面', 'admin_rbac_role_add_view', 'admin', '后台角色添加界面');
INSERT INTO `cmf_hook` VALUES (70, 2, 1, '后台角色编辑界面', 'admin_rbac_role_edit_view', 'admin', '后台角色编辑界面');
INSERT INTO `cmf_hook` VALUES (71, 2, 1, '后台角色授权界面', 'admin_rbac_authorize_view', 'admin', '后台角色授权界面');

-- ----------------------------
-- Table structure for cmf_hook_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_hook_plugin`;
CREATE TABLE `cmf_hook_plugin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `hook` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '钩子名',
  `plugin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统钩子插件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_hook_plugin
-- ----------------------------
INSERT INTO `cmf_hook_plugin` VALUES (1, 10000, 1, 'fetch_upload_view', 'Qiniu');

-- ----------------------------
-- Table structure for cmf_level
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level`;
CREATE TABLE `cmf_level`  (
  `levelid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `levelname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '等级名称',
  `level_up` int(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经验上限',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标识',
  `colour` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称颜色',
  `thumb_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像角标',
  `bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '背景',
  PRIMARY KEY (`id`, `levelid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_level
-- ----------------------------
INSERT INTO `cmf_level` VALUES (1, '一级', 999, 1457923458, 1, 'level_1.png', 'b2d65e', 'level_mark_1.png', 'level_bg_1.png');
INSERT INTO `cmf_level` VALUES (2, '二级', 1999, 1459240543, 2, 'level_2.png', 'b2d65e', 'level_mark_2.png', 'level_bg_2.png');
INSERT INTO `cmf_level` VALUES (3, '三级', 2999, 1459240560, 3, 'level_3.png', 'b2d65e', 'level_mark_3.png', 'level_bg_3.png');
INSERT INTO `cmf_level` VALUES (4, '四级', 3999, 1459240618, 4, 'level_4.png', 'b2d65e', 'level_mark_4.png', 'level_bg_4.png');
INSERT INTO `cmf_level` VALUES (5, '五级', 19999, 1459240659, 5, 'level_5.png', 'b2d65e', 'level_mark_5.png', 'level_bg_5.png');
INSERT INTO `cmf_level` VALUES (6, '六级', 29999, 1459240684, 6, 'level_6.png', 'ffb500', 'level_mark_6.png', 'level_bg_6.png');
INSERT INTO `cmf_level` VALUES (7, '七级', 39999, 1463802287, 7, 'level_7.png', 'ffb500', 'level_mark_7.png', 'level_bg_7.png');
INSERT INTO `cmf_level` VALUES (8, '八级', 49999, 1463980529, 8, 'level_8.png', 'ffb500', 'level_mark_8.png', 'level_bg_8.png');
INSERT INTO `cmf_level` VALUES (9, '九级', 69999, 1463980683, 9, 'level_9.png', 'ffb500', 'level_mark_9.png', 'level_bg_9.png');
INSERT INTO `cmf_level` VALUES (10, '十级', 99999, 1465371136, 10, 'level_10.png', 'ffb500', 'level_mark_10.png', 'level_bg_10.png');

-- ----------------------------
-- Table structure for cmf_level_anchor
-- ----------------------------
DROP TABLE IF EXISTS `cmf_level_anchor`;
CREATE TABLE `cmf_level_anchor`  (
  `levelid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '等级',
  `levelname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '等级名称',
  `level_up` int(20) UNSIGNED NULL DEFAULT 0 COMMENT '等级上限',
  `addtime` int(11) NULL DEFAULT 0 COMMENT '添加时间',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标识',
  `thumb_mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像角标',
  `bg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '背景',
  PRIMARY KEY (`id`, `levelid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_level_anchor
-- ----------------------------
INSERT INTO `cmf_level_anchor` VALUES (1, '一级', 399, 1457923458, 1, 'level_anchor_1.png', 'level_anchor_mark_1.png', 'level_anchor_bg_1.png');
INSERT INTO `cmf_level_anchor` VALUES (2, '二级', 599, 1459240543, 2, 'level_anchor_2.png', 'level_anchor_mark_2.png', 'level_anchor_bg_2.png');
INSERT INTO `cmf_level_anchor` VALUES (3, '三级', 799, 1459240560, 3, 'level_anchor_3.png', 'level_anchor_mark_3.png', 'level_anchor_bg_3.png');
INSERT INTO `cmf_level_anchor` VALUES (4, '四级', 999, 1459240618, 4, 'level_anchor_4.png', 'level_anchor_mark_4.png', 'level_anchor_bg_4.png');
INSERT INTO `cmf_level_anchor` VALUES (5, '五级', 1999, 1459240659, 5, 'level_anchor_5.png', 'level_anchor_mark_5.png', 'level_anchor_bg_5.png');

-- ----------------------------
-- Table structure for cmf_link
-- ----------------------------
DROP TABLE IF EXISTS `cmf_link`;
CREATE TABLE `cmf_link`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:显示;0:不显示',
  `rating` int(11) NOT NULL DEFAULT 0 COMMENT '友情链接评级',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接描述',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接地址',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '友情链接名称',
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接图标',
  `target` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `rel` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '链接与网站的关系',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '友情链接表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_live
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live`;
CREATE TABLE `cmf_live`  (
  `uid` int(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `showid` int(12) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `islive` int(1) NOT NULL DEFAULT 0 COMMENT '直播状态',
  `starttime` int(12) NOT NULL DEFAULT 0 COMMENT '开播时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `stream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '流名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图',
  `pull` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '播流地址',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '维度',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播类型',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '类型值',
  `isvideo` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否假视频',
  `anyway` tinyint(1) NOT NULL DEFAULT 1 COMMENT '横竖屏，0表示竖屏，1表示横屏',
  `liveclassid` int(11) NOT NULL DEFAULT 0 COMMENT '直播分类ID',
  `hotvotes` int(11) NOT NULL DEFAULT 0 COMMENT '热门礼物总额',
  `deviceinfo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备信息',
  PRIMARY KEY (`uid`) USING BTREE,
  INDEX `index_islive_starttime`(`islive`, `starttime`) USING BTREE,
  INDEX `index_uid_stream`(`uid`, `stream`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_live_class
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_class`;
CREATE TABLE `cmf_live_class`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `des` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '描述',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_class
-- ----------------------------
INSERT INTO `cmf_live_class` VALUES (1, '音乐', 'liveclass_1.png', '流行、摇滚、说唱、民族等，线上最强演唱会', 13);
INSERT INTO `cmf_live_class` VALUES (4, '校园', 'liveclass_4.png', '学生党分享校园乐事', 4);
INSERT INTO `cmf_live_class` VALUES (5, '交友', 'liveclass_5.png', '单身男女聚集地，线上交友趣事多', 5);
INSERT INTO `cmf_live_class` VALUES (9, '美食', 'liveclass_9.png', '吃货？主厨？唯美食不可辜负', 9);
INSERT INTO `cmf_live_class` VALUES (10, '才艺', 'liveclass_10.png', '手工艺、魔术、画画、化妆等，艺术高手在民间', 10);
INSERT INTO `cmf_live_class` VALUES (13, '男神', 'qiniu_admin/20220219/f24f63e8be1043769f51091d5bf6eba6.png', '前方有一大波迷妹即将赶到', 9999);

-- ----------------------------
-- Table structure for cmf_live_record
-- ----------------------------
DROP TABLE IF EXISTS `cmf_live_record`;
CREATE TABLE `cmf_live_record`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `showid` int(11) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `nums` int(11) NOT NULL DEFAULT 0 COMMENT '关播时人数',
  `starttime` int(11) NOT NULL DEFAULT 0 COMMENT '开始时间',
  `endtime` int(11) NOT NULL DEFAULT 0 COMMENT '结束时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `stream` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '流名',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '封面图',
  `lng` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '经度',
  `lat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '纬度',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播类型',
  `type_val` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '类型值',
  `votes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '本次直播收益',
  `time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '格式日期',
  `liveclassid` int(11) NOT NULL DEFAULT 0 COMMENT '直播分类ID',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '回放地址',
  `live_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '直播类型 0 视频直播 1 语音聊天室',
  `deviceinfo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_uid_start`(`uid`, `starttime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_live_record
-- ----------------------------
INSERT INTO `cmf_live_record` VALUES (1, 3, 1644891308, 0, 1644891308, 1644891365, '', '', '好像在火星', '3_1644891308', '', '', '', 0, '0', '0', '2022-02-15', 0, '', 0, 'OPPO_PDAM10_11_8GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (2, 3, 1644908204, 0, 1644908204, 1644908225, '', '', '好像在火星', '3_1644908204', '', '', '', 0, '0', '0', '2022-02-15', 0, '', 0, 'OPPO_PDAM10_11_8GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (3, 3, 1644908624, 0, 1644908624, 1644908632, '', '', '好像在火星', '3_1644908624', '', '', '', 0, '0', '0', '2022-02-15', 9, '', 0, 'OPPO_PDAM10_11_8GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (4, 3, 1644977909, 0, 1644977909, 1644977938, '', '', '好像在火星', '3_1644977909', '', '', '', 0, '0', '0', '2022-02-16', 0, '', 0, 'OPPO_PDAM10_11_8GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (5, 3, 1644996774, 1, 1644996774, 1644997649, '急急急急急急那你', '', '好像在火星', '3_1644996774', '', '', '', 1, '123', '1624.00', '2022-02-16', 0, '', 0, 'OPPO_PDAM10_11_8GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (6, 5, 1645088759, 0, 1645088759, 1645088810, '', '', '好像在火星', '5_1645088759', '', '', '', 0, '0', '0', '2022-02-17', 4, '', 0, 'HONOR_MOA-AL00_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (7, 6, 1645090275, 0, 1645090275, 1645090849, '', '', '好像在火星', '6_1645090275', '', '', '', 0, '0', '0', '2022-02-17', 5, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (8, 6, 1645090866, 0, 1645090866, 1645090900, '', '', '好像在火星', '6_1645090866', '', '', '', 0, '0', '0', '2022-02-17', 5, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (9, 5, 1645144879, 0, 1645144879, 1645145048, '', '', '好像在火星', '5_1645144879', '', '', '', 0, '0', '0', '2022-02-18', 0, '', 0, 'HONOR_MOA-AL00_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (10, 6, 1645147659, 0, 1645147659, 1645147753, '', '', '好像在火星', '6_1645147659', '', '', '', 0, '0', '0', '2022-02-18', 4, '', 0, '');
INSERT INTO `cmf_live_record` VALUES (11, 6, 1645147814, 0, 1645147814, 1645147927, '', '', '好像在火星', '6_1645147814', '', '', '', 0, '0', '0', '2022-02-18', 0, '', 0, '');
INSERT INTO `cmf_live_record` VALUES (12, 6, 1645147960, 0, 1645147960, 1645147969, '', '', '好像在火星', '6_1645147960', '', '', '', 0, '0', '0', '2022-02-18', 4, '', 0, '');
INSERT INTO `cmf_live_record` VALUES (13, 6, 1645148113, 0, 1645148113, 1645148128, '', '', '好像在火星', '6_1645148113', '', '', '', 0, '0', '0', '2022-02-18', 0, '', 0, '');
INSERT INTO `cmf_live_record` VALUES (14, 6, 1645149892, 0, 1645149892, 1645149906, '', '', '好像在火星', '6_1645149892', '', '', '', 0, '0', '0', '2022-02-18', 0, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (15, 7, 1645152871, 1, 1645152871, 1645153128, '', '', '好像在火星', '7_1645152871', '', '', '', 0, '0', '1532.00', '2022-02-18', 0, '', 0, 'HONOR_MOA-AL00_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (16, 9, 1645153683, 1, 1645153683, 1645153722, '', '', '好像在火星', '9_1645153683', '', '', '', 1, '1', '0', '2022-02-18', 0, '', 0, '');
INSERT INTO `cmf_live_record` VALUES (17, 9, 1645153741, 0, 1645153741, 1645153899, '', '', '好像在火星', '9_1645153741', '', '', '', 2, '20', '520.00', '2022-02-18', 10, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (18, 9, 1645153908, 0, 1645153908, 1645153942, '', '', '好像在火星', '9_1645153908', '', '', '', 3, '20', '540.00', '2022-02-18', 10, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (19, 10, 1645175642, 0, 1645175642, 1645176890, '', '', '好像在火星', '10_1645175642', '', '', '', 0, '0', '311.00', '2022-02-18', 0, '', 0, 'HONOR_MOA-AL00_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (20, 8, 1645250593, 0, 1645250593, 1645250998, '', '', '好像在火星', '8_1645250593', '', '', '', 0, '0', '0', '2022-02-19', 9, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (21, 8, 1645251973, 0, 1645251973, 1645252011, '', '', '好像在火星', '8_1645251973', '', '', '', 0, '0', '0', '2022-02-19', 10, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');
INSERT INTO `cmf_live_record` VALUES (22, 8, 1645254228, 0, 1645254228, 1645255911, '', '', '好像在火星', '8_1645254228', '', '', '', 0, '0', '10.00', '2022-02-19', 9, '', 0, 'Xiaomi_Redmi 8A_10_4GB_WIFI');

-- ----------------------------
-- Table structure for cmf_option
-- ----------------------------
DROP TABLE IF EXISTS `cmf_option`;
CREATE TABLE `cmf_option`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `autoload` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否自动加载;1:自动加载;0:不自动加载',
  `option_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置名',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '配置值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `option_name`(`option_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '全站配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_option
-- ----------------------------
INSERT INTO `cmf_option` VALUES (1, 1, 'site_info', '{\"maintain_switch\":\"0\",\"maintain_tips\":\"\\u7ef4\\u62a4\\u901a\\u77e5\\uff1a\\u4e3a\\u4e86\\u66f4\\u597d\\u7684\\u4e3a\\u60a8\\u670d\\u52a1\\uff0c\\u672c\\u7ad9\\u6b63\\u5728\\u5347\\u7ea7\\u7ef4\\u62a4\\u4e2d\\uff0c\\u56e0\\u6b64\\u5e26\\u6765\\u4e0d\\u4fbf\\u6df1\\u8868\\u6b49\\u610f\",\"company_name\":\"\",\"company_desc\":\"\",\"site_name\":\"\\u4e91\\u8c79\\u76f4\\u64ad\\u7cfb\\u7edf\",\"site\":\"http:\\/\\/xxxx.com\",\"copyright\":\"\\u7248\\u6743\\u4fe1\\u606f\",\"copyright_url\":\"\",\"name_coin\":\"\\u94bb\\u77f3\",\"name_votes\":\"\\u6620\\u7968\",\"site_seo_title\":\"\",\"site_seo_keywords\":\"\",\"site_seo_description\":\"\",\"isup\":\"0\",\"apk_ver\":\"1.0.4\",\"apk_url\":\"3333\",\"apk_des\":\"23\",\"qr_url\":\"\",\"live_time_coin\":\"20,30,40,50,60,70\",\"login_alert_title\":\"\\u670d\\u52a1\\u534f\\u8bae\\u548c\\u9690\\u79c1\\u653f\\u7b56\",\"login_alert_content\":\"\\u8bf7\\u60a8\\u52a1\\u5fc5\\u4ed4\\u7ec6\\u9605\\u8bfb\\uff0c\\u5145\\u5206\\u7406\\u89e3\\u670d\\u52a1\\u534f\\u8bae\\u548c\\u9690\\u79c1\\u653f\\u7b56\\u5404\\u6761\\u6b3e\\uff0c\\u5305\\u62ec\\u4f46\\u4e0d\\u9650\\u4e8e\\u4e3a\\u4e86\\u5411\\u60a8\\u63d0\\u4f9b\\u5373\\u65f6\\u901a\\u8baf\\uff0c\\u5185\\u5bb9\\u5206\\u4eab\\u7b49\\u670d\\u52a1\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u6536\\u96c6\\u60a8\\u8bbe\\u5907\\u4fe1\\u606f\\u548c\\u4e2a\\u4eba\\u4fe1\\u606f\\uff0c\\u60a8\\u53ef\\u4ee5\\u5728\\u8bbe\\u7f6e\\u4e2d\\u67e5\\u770b\\uff0c\\u7ba1\\u7406\\u60a8\\u7684\\u6388\\u6743\\u3002\\u60a8\\u53ef\\u9605\\u8bfb\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\\u4e86\\u89e3\\u8be6\\u7ec6\\u4fe1\\u606f\\uff0c\\u5982\\u60a8\\u540c\\u610f\\uff0c\\u8bf7\\u70b9\\u51fb\\u540c\\u610f\\u63a5\\u53d7\\u6211\\u4eec\\u7684\\u670d\\u52a1\\u3002\",\"login_clause_title\":\"\\u6211\\u5df2\\u8be6\\u7ec6\\u9605\\u8bfb\\u5e76\\u540c\\u610f\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\\u548c\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_private_title\":\"\\u300a\\u9690\\u79c1\\u653f\\u7b56\\u300b\",\"login_private_url\":\"\\/portal\\/page\\/index?id=3\",\"login_service_title\":\"\\u300a\\u670d\\u52a1\\u534f\\u8bae\\u300b\",\"login_service_url\":\"\\/portal\\/page\\/index?id=4\",\"login_type\":\"qq,wx\",\"live_type\":\"0;\\u666e\\u901a\\u623f\\u95f4,1;\\u5bc6\\u7801\\u623f\\u95f4,2;\\u95e8\\u7968\\u623f\\u95f4,3;\\u8ba1\\u65f6\\u623f\\u95f4\"}');
INSERT INTO `cmf_option` VALUES (2, 1, 'cmf_settings', '{\"banned_usernames\":\"\"}');
INSERT INTO `cmf_option` VALUES (3, 1, 'cdn_settings', '{\"cdn_static_root\":\"\"}');
INSERT INTO `cmf_option` VALUES (4, 1, 'admin_settings', '{\"admin_password\":\"\",\"admin_theme\":\"admin_simpleboot3\",\"admin_style\":\"flatadmin\"}');
INSERT INTO `cmf_option` VALUES (5, 1, 'storage', '{\"storages\":{\"Qiniu\":{\"name\":\"\\u4e03\\u725b\\u4e91\\u5b58\\u50a8\",\"driver\":\"\\\\plugins\\\\qiniu\\\\lib\\\\Qiniu\"}},\"type\":\"Qiniu\"}');
INSERT INTO `cmf_option` VALUES (6, 1, 'configpri', '{\"sensitive_words\":\"\\u6bdb\\u6cfd\\u4e1c,\\u4e60\\u8fd1\\u5e73,\\u80e1\\u9526\\u6d9b,\\u6c5f\\u6cfd\\u6c11,\\u6731\\u9555\\u57fa,\\u4e14,weixin,qq,\\u5fae\\u4fe1,QQ,\\u50bb\\u903c\",\"reg_reward\":\"999999\",\"sendcode_switch\":\"0\",\"tencent_sms_appid\":\"\",\"tencent_sms_appkey\":\"\",\"tencent_sms_signName\":\"\",\"tencent_sms_templateCode\":\"\",\"iplimit_switch\":\"0\",\"iplimit_times\":\"10\",\"auth_islimit\":\"0\",\"level_islimit\":\"0\",\"level_limit\":\"6\",\"speak_limit\":\"0\",\"barrage_limit\":\"0\",\"barrage_fee\":\"10\",\"userlist_time\":\"60\",\"chatserver\":\"http:\\/\\/xxxx.com:19975\",\"cdn_switch\":\"2\",\"tx_appid\":\"\",\"tx_bizid\":\"\",\"tx_push_key\":\"\",\"tx_acc_key\":\"\",\"tx_api_key\":\"\",\"tx_play_key\":\"\",\"tx_play_time\":\"\",\"tx_play_key_switch\":\"0\",\"tx_push\":\"\",\"tx_pull\":\"\",\"cash_rate\":\"100\",\"cash_take\":\"5\",\"cash_min\":\"10\",\"cash_start\":\"1\",\"cash_end\":\"20\",\"cash_max_times\":\"0\",\"aliapp_partner\":\"\",\"aliapp_seller_id\":\"\",\"aliapp_key_android\":\"\",\"wx_appid\":\"\",\"wx_appsecret\":\"\",\"wx_mchid\":\"\",\"wx_key\":\"\",\"aliapp_switch\":\"1\",\"wx_switch\":\"1\",\"cloudtype\":\"1\"}');
INSERT INTO `cmf_option` VALUES (7, 1, 'jackpot', '{\"switch\":\"1\",\"luck_anchor\":\"60\",\"luck_jackpot\":\"20\"}');
INSERT INTO `cmf_option` VALUES (8, 1, 'guide', '{\"switch\":\"1\",\"type\":\"0\",\"time\":\"3\"}');
INSERT INTO `cmf_option` VALUES (9, 1, 'upload_setting', '{\"max_files\":\"20\",\"chunk_size\":\"512\",\"file_types\":{\"image\":{\"upload_max_filesize\":\"10240\",\"extensions\":\"jpg,jpeg,png,gif,bmp4,svga\"},\"video\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"mp4\"},\"audio\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"mp3\"},\"file\":{\"upload_max_filesize\":\"102400\",\"extensions\":\"txt,pdf,doc,docx,xls,xlsx,ppt,pptx,svga,mp4\"}}}');

-- ----------------------------
-- Table structure for cmf_plugin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_plugin`;
CREATE TABLE `cmf_plugin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '插件类型;1:网站;8:微信',
  `has_admin` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否有后台管理,0:没有;1:有',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:开启;0:禁用',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '插件安装时间',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件标识名,英文字母(惟一)',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件名称',
  `demo_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `hooks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实现的钩子;以“,”分隔',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '插件作者',
  `author_url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `version` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '插件版本号',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '插件描述',
  `config` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '插件配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '插件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_plugin
-- ----------------------------
INSERT INTO `cmf_plugin` VALUES (1, 1, 0, 1, 0, 'Qiniu', '七牛云存储', '', '', 'ThinkCMF', '', '1.0.1', 'ThinkCMF七牛专享优惠码:507670e8', '{\"accessKey\":\" \",\"secretKey\":\" \",\"protocol\":\"http\",\"domain\":\" \",\"bucket\":\" \",\"zone\":\"z0\"}');

-- ----------------------------
-- Table structure for cmf_portal_category
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category`;
CREATE TABLE `cmf_portal_category`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类父id',
  `post_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类文章数',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布,0:不发布',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类描述',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类层级关系路径',
  `seo_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `seo_keywords` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `seo_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `list_tpl` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类列表模板',
  `one_tpl` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '分类文章页模板',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展属性',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_category
-- ----------------------------
INSERT INTO `cmf_portal_category` VALUES (1, 0, 0, 0, 0, 10000, 'ceshi1', '', '0-1', '', '', '', '', '', '{\"thumbnail\":\"\"}');
INSERT INTO `cmf_portal_category` VALUES (2, 0, 0, 1, 1622973900, 10000, '111', '', '0-2', '', '', '', '', '', '{\"thumbnail\":\"\"}');

-- ----------------------------
-- Table structure for cmf_portal_category_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_category_post`;
CREATE TABLE `cmf_portal_category_post`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章id',
  `category_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分类id',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `term_taxonomy_id`(`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'portal应用 分类文章对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_category_post
-- ----------------------------
INSERT INTO `cmf_portal_category_post` VALUES (1, 2, 1, 10000, 0);

-- ----------------------------
-- Table structure for cmf_portal_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_post`;
CREATE TABLE `cmf_portal_post`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `post_type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '类型,1:文章;2:页面',
  `post_format` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '内容格式;1:html;2:md',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发表者用户id',
  `post_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态;1:已发布;0:未发布;',
  `comment_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '评论状态;1:允许;0:不允许',
  `is_top` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否置顶;1:置顶;0:不置顶',
  `recommended` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_hits` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '查看数',
  `post_favorites` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '收藏数',
  `post_like` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数',
  `comment_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `published_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发布时间',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `post_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'post标题',
  `post_keywords` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'seo keywords',
  `post_excerpt` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'post摘要',
  `post_source` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '转载文章的来源',
  `thumbnail` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '缩略图',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '文章内容',
  `post_content_filtered` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '处理过的文章内容',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展属性,如缩略图;格式为json',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '页面类型，0单页面，2关于我们',
  `list_order` int(11) NOT NULL DEFAULT 9999 COMMENT '序号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_status_date`(`post_type`, `post_status`, `create_time`, `id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `create_time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_portal_post
-- ----------------------------
INSERT INTO `cmf_portal_post` VALUES (2, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575612912, 1603088562, 1575612840, 0, '社区公约', '', '', '', '', '&lt;p&gt;  社区公约，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 1);
INSERT INTO `cmf_portal_post` VALUES (3, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575612937, 1589269790, 1575612900, 0, '隐私政策', '', '', '', '', '\n&lt;p style=&quot;white-space: normal;&quot;&gt;  隐私政策，内容可在管理后台设置。&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (4, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575612961, 1589269754, 1575612900, 0, '服务协议', '', '', '', '', '&lt;p&gt;  服务协议，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (5, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575612983, 1578534055, 1575612960, 0, '联系我们', '', '', '', '', '\n&lt;p style=&quot;margin-top: 0px; margin-bottom: 20px; white-space: normal; padding: 0px; font-size: 14px; line-height: 25.2px; text-indent: 28px; color: rgb(85, 85, 85); font-family: arial, &quot;&gt;联系我们，内容可在管理后台设置。&lt;/p&gt;\n&lt;p&gt;&lt;br&gt;&lt;/p&gt;\n', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (6, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613058, 1591087668, 1575613020, 0, '用户充值协议', '', '', '', '', '&lt;p&gt;用户充值协议用户充值协议用户充值协议用户充值协议&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);
INSERT INTO `cmf_portal_post` VALUES (10, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613012, 1578534178, 1575612960, 0, '主播协议', '', '', '', '', '&lt;p&gt;   主播协议，内容可在管理后台设置。&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 2, 9999);
INSERT INTO `cmf_portal_post` VALUES (18, 0, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1575613036, 1599550227, 1575612960, 0, '签约说明', '', '', '', '', '&lt;p&gt;签约说明&lt;/p&gt;', NULL, '{\"thumbnail\":\"\",\"template\":\"\"}', 0, 9999);

-- ----------------------------
-- Table structure for cmf_portal_tag
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag`;
CREATE TABLE `cmf_portal_tag`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布,0:不发布',
  `recommended` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否推荐;1:推荐;0:不推荐',
  `post_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标签文章数',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'portal应用 文章标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_portal_tag_post
-- ----------------------------
DROP TABLE IF EXISTS `cmf_portal_tag_post`;
CREATE TABLE `cmf_portal_tag_post`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标签 id',
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文章 id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:发布;0:不发布',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `post_id`(`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'portal应用 标签文章对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_praise_messages
-- ----------------------------
DROP TABLE IF EXISTS `cmf_praise_messages`;
CREATE TABLE `cmf_praise_messages`  (
  `id` int(12) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户id',
  `touid` int(12) NOT NULL DEFAULT 0 COMMENT '被赞用户id',
  `obj_id` int(12) NOT NULL DEFAULT 0 COMMENT '被操作对象id',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '被操作类型 0评论 1视频',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '操作时间',
  `video_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '视频封面',
  `videoid` int(12) NOT NULL DEFAULT 0 COMMENT '视频id',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否显示 0否 1 是  （根据视频是否下架决定）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cmf_recycle_bin
-- ----------------------------
DROP TABLE IF EXISTS `cmf_recycle_bin`;
CREATE TABLE `cmf_recycle_bin`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NULL DEFAULT 0 COMMENT '删除内容 id',
  `create_time` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '创建时间',
  `table_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '删除内容所在表名',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '删除内容名称',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = ' 回收站' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_role
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role`;
CREATE TABLE `cmf_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父角色ID',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '状态;0:禁用;1:正常',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间',
  `list_order` float NOT NULL DEFAULT 0 COMMENT '排序',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_role
-- ----------------------------
INSERT INTO `cmf_role` VALUES (1, 0, 1, 1329633709, 1329633709, 0, '超级管理员', '拥有网站最高管理员权限！');
INSERT INTO `cmf_role` VALUES (2, 0, 1, 0, 0, 0, '普通', '');

-- ----------------------------
-- Table structure for cmf_role_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role_user`;
CREATE TABLE `cmf_role_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色 id',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色对应表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_route
-- ----------------------------
DROP TABLE IF EXISTS `cmf_route`;
CREATE TABLE `cmf_route`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态;1:启用,0:不启用',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'URL规则类型;1:用户自定义;2:别名添加',
  `full_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '完整url',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '实际显示的url',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'url路由表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_slide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide`;
CREATE TABLE `cmf_slide`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:显示,0不显示',
  `delete_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除时间',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片分类',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '分类备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '幻灯片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_slide
-- ----------------------------
INSERT INTO `cmf_slide` VALUES (2, 1, 0, 'APP首页轮播', '不可删除');
INSERT INTO `cmf_slide` VALUES (3, 1, 1596874182, ' ', '');
INSERT INTO `cmf_slide` VALUES (4, 1, 1596878259, ' ', '');

-- ----------------------------
-- Table structure for cmf_slide_item
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide_item`;
CREATE TABLE `cmf_slide_item`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slide_id` int(11) NOT NULL DEFAULT 0 COMMENT '幻灯片id',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态,1:显示;0:隐藏',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片图片',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片链接',
  `target` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '友情链接打开方式',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '幻灯片描述',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '幻灯片内容',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `slide_id`(`slide_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '幻灯片子项表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_slide_item
-- ----------------------------
INSERT INTO `cmf_slide_item` VALUES (1, 2, 1, 10000, '直播', 'admin/20220219/6097e26083e49afcf51920241a77804c.jpg', '直播', '', '直播', '', NULL);
INSERT INTO `cmf_slide_item` VALUES (2, 2, 1, 10000, '主播直播', 'admin/20220219/6ad916d6749af54f8df7f42cb8f6db48.jpg', '', '', '直播', '', NULL);

-- ----------------------------
-- Table structure for cmf_theme
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme`;
CREATE TABLE `cmf_theme`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '安装时间',
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后升级时间',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模板状态,1:正在使用;0:未使用',
  `is_compiled` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为已编译模板',
  `theme` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题目录名，用于主题的维一标识',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题名称',
  `version` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题版本号',
  `demo_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '演示地址，带协议',
  `thumbnail` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '缩略图',
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题作者',
  `author_url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '作者网站链接',
  `lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支持语言',
  `keywords` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题关键字',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_theme
-- ----------------------------
INSERT INTO `cmf_theme` VALUES (1, 0, 0, 0, 0, 'default', 'default', '1.0.0', 'http://demo.thinkcmf.com', '', 'ThinkCMF', 'http://www.thinkcmf.com', 'zh-cn', 'ThinkCMF默认模板', 'ThinkCMF默认模板');

-- ----------------------------
-- Table structure for cmf_theme_file
-- ----------------------------
DROP TABLE IF EXISTS `cmf_theme_file`;
CREATE TABLE `cmf_theme_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_public` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否公共的模板文件',
  `list_order` float NOT NULL DEFAULT 10000 COMMENT '排序',
  `theme` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板名称',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件名',
  `action` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作',
  `file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件，相对于模板根目录，如Portal/index.html',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '模板文件描述',
  `more` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板更多配置,用户自己后台设置的',
  `config_more` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板更多配置,来源模板的配置文件',
  `draft_more` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '模板更多配置,用户临时保存的配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user`;
CREATE TABLE `cmf_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户类型;1:admin;2:会员',
  `sex` tinyint(2) NOT NULL DEFAULT 0 COMMENT '性别;0:保密,1:男,2:女',
  `birthday` int(11) NOT NULL DEFAULT 0 COMMENT '生日',
  `last_login_time` int(11) NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `score` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户积分',
  `coin` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '金币',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `user_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户状态;0:禁用,1:正常,2:未验证',
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录密码;cmf_password加密',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户登录邮箱',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户个人网址',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `avatar_thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '小头像',
  `signature` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '个性签名',
  `last_login_ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `user_activation_key` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '激活码',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '中国手机不带国家代码，国际手机号格式为：国家代码-手机号',
  `more` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展属性',
  `consumption` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '消费总额',
  `votes` decimal(20, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '映票余额',
  `votestotal` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '映票总额',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '城市',
  `openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '三方标识',
  `login_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'phone' COMMENT '注册方式',
  `ishot` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否热门显示',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'pc' COMMENT '注册来源',
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所在地',
  `recommend_time` int(1) NOT NULL DEFAULT 0 COMMENT '推荐时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_login`(`user_login`) USING BTREE,
  INDEX `user_nicename`(`user_nicename`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user
-- ----------------------------
INSERT INTO `cmf_user` VALUES (1, 1, 0, 0, 1645410121, 0, 0, 1571800517, 1, 'admin', '###66bc82ebde1af7c7a129adf2a2eb4f57', 'admin', 'admin@163.com', '', '', '', '', '112.245.91.61', '', '', NULL, 0, 0.00, 0, '', '', '', 'phone', 1, 'pc', '', 0);
INSERT INTO `cmf_user` VALUES (2, 2, 1, 0, 0, 0, 0, 1644567476, 1, '15011111111', '###ea1e6729b1bcd5575033baf0e8cd127b', '呀哈', '', '', '/default.jpg', '/default_thumb.jpg', ' 1', '', '', '15011111111', NULL, 0, 20266.00, 20266, '', '', '', 'phone', 1, 'pc', '', 0);
INSERT INTO `cmf_user` VALUES (3, 2, 1, 1644940800, 1645170334, 0, 995102, 1644650413, 1, 'qq_1644650413208', '###69ab35392243263c399550cf918f191b', '网站架构师', '', '', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=anEY1l6fMa1MMjZBzYLPXQ&amp;s=100&amp;t=1634789779', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=anEY1l6fMa1MMjZBzYLPXQ&amp;s=100&amp;t=1634789779', '这家伙很懒，什么都没留下', '112.245.91.61', '', '', NULL, 4897, 1624.00, 1624, '', '', '6B9970C3526810B4F24361B3601564C5', 'qq', 1, 'android', '北京市北京市东城区', 0);
INSERT INTO `cmf_user` VALUES (4, 2, 0, 0, 1645408760, 0, 986732, 1644978064, 1, 'qq_1644978064775', '###69ab35392243263c399550cf918f191b', '寒水依痕', '', '', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=VicFDACiaBRGte5jaiaInLpOQ&amp;s=100&amp;t=1554802375', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=VicFDACiaBRGte5jaiaInLpOQ&amp;s=100&amp;t=1554802375', '这家伙很懒，什么都没留下', '112.250.5.105', '', '', NULL, 13267, 0.00, 0, '', '', '111CE8121D052032C5420EEAAC07244F', 'qq', 1, 'android', '', 0);
INSERT INTO `cmf_user` VALUES (5, 2, 0, 0, 1645171570, 0, 0, 1645088407, 1, 'qq_1645088407892', '###69ab35392243263c399550cf918f191b', '云豹科技', '', '', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=iaA7I09XqFFMDIB0SEX6Q1Q&amp;s=100&amp;t=1555680311', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=iaA7I09XqFFMDIB0SEX6Q1Q&amp;s=100&amp;t=1555680311', '这家伙很懒，什么都没留下', '112.245.91.61', '', '', NULL, 0, 0.00, 0, '', '', '8A53A4E3073BDCB19D86CBFE3FA0724A', 'qq', 1, 'android', '', 0);
INSERT INTO `cmf_user` VALUES (6, 2, 0, 0, 1645090263, 0, 0, 1645090263, 1, 'qq_1645090263457', '###69ab35392243263c399550cf918f191b', '云豹-呀哈', '', '', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=IjDibyj5GdeciabeZvdEvfSA&amp;s=100&amp;t=1644982898', 'http://thirdqq.qlogo.cn/g?b=oidb&amp;k=IjDibyj5GdeciabeZvdEvfSA&amp;s=100&amp;t=1644982898', '这家伙很懒，什么都没留下', '112.245.91.61', '', '', NULL, 0, 0.00, 0, '', '', 'F7A66540BDE8425EF72509A581BBF670', 'qq', 1, 'android', '', 0);
INSERT INTO `cmf_user` VALUES (7, 2, 1, 730051200, 1645254226, 0, 998099, 1645151074, 1, '15314090000', '###ea1e6729b1bcd5575033baf0e8cd127b', '哈哈哈哈', '', '', '/default.jpg', '/default_thumb.jpg', '爱你', '112.245.91.61', '', '15314090000', NULL, 1901, 1532.00, 1532, '', '', '', 'phone', 1, 'pc', '北京市北京市东城区', 0);
INSERT INTO `cmf_user` VALUES (8, 2, 0, 0, 1645249138, 0, 9998999, 1645151129, 1, '15314091111', '###ea1e6729b1bcd5575033baf0e8cd127b', '手机用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.245.91.61', '', '15314091111', NULL, 1000, 10.00, 10, '', '', '', 'phone', 1, 'android', '', 0);
INSERT INTO `cmf_user` VALUES (9, 2, 2, 856195200, 1645151258, 0, 98467, 1645151258, 1, '15314091112', '###ea1e6729b1bcd5575033baf0e8cd127b', '金钟大', '', '', 'qiniu_android_9_20220218_110518_0529992.jpg?imageView2/2/w/600/h/600', 'qiniu_android_9_20220218_110518_0529992.jpg?imageView2/2/w/200/h/200', '爱就一个字', '112.245.91.61', '', '15314091112', NULL, 1532, 60.00, 1060, '', '', '', 'phone', 1, 'android', '北京市北京市东城区', 0);
INSERT INTO `cmf_user` VALUES (10, 2, 0, 0, 1645238096, 0, 8795, 1645175068, 1, '15311111111', '###ea1e6729b1bcd5575033baf0e8cd127b', '手机用户1111', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.245.91.61', '', '15311111111', NULL, 1204, 311.00, 311, '', '', '', 'phone', 1, 'android', '', 0);
INSERT INTO `cmf_user` VALUES (11, 2, 0, 0, 1645263655, 0, 8997, 1645230563, 1, '15314092222', '###ea1e6729b1bcd5575033baf0e8cd127b', '手机用户2222', '', '', '/default.jpg', '/default_thumb.jpg', '这家伙很懒，什么都没留下', '112.245.91.61', '', '15314092222', NULL, 1002, 0.00, 0, '', '', '', 'phone', 1, 'android', '', 0);

-- ----------------------------
-- Table structure for cmf_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_auth`;
CREATE TABLE `cmf_user_auth`  (
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `real_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `mobile` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电话',
  `cer_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '身份证号',
  `front_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '正面',
  `back_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '反面',
  `handset_view` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手持',
  `reason` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '审核说明',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '提交时间',
  `uptime` int(12) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态 0 处理中 1 成功 2 失败',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_auth
-- ----------------------------
INSERT INTO `cmf_user_auth` VALUES (9, '周五', '15314090000', '370911199703226843', 'qiniu_android_9_20220218_110128_466609.jpg', 'qiniu_android_9_20220218_110128_949547.jpg', 'qiniu_android_9_20220218_110129_3326472.jpg', '', 1645153288, 1645153321, 1);

-- ----------------------------
-- Table structure for cmf_user_banrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_banrecord`;
CREATE TABLE `cmf_user_banrecord`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ban_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '被禁用原因',
  `ban_long` int(10) NULL DEFAULT 0 COMMENT '用户禁用时长：单位：分钟',
  `uid` int(10) NULL DEFAULT 0 COMMENT '禁用 用户ID',
  `addtime` int(10) NULL DEFAULT 0 COMMENT '提交时间',
  `end_time` int(10) NULL DEFAULT 0 COMMENT '禁用到期时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_beauty_params
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_beauty_params`;
CREATE TABLE `cmf_user_beauty_params`  (
  `uid` bigint(20) NOT NULL COMMENT '用户id',
  `params` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '美颜参数json字符串',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_black
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_black`;
CREATE TABLE `cmf_user_black`  (
  `uid` int(12) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(12) NOT NULL DEFAULT 0 COMMENT '被拉黑人ID',
  INDEX `uid_touid_index`(`uid`, `touid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_coinrecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_coinrecord`;
CREATE TABLE `cmf_user_coinrecord`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为，1赠送礼物,2弹幕,3登录奖励,4购买VIP,5购买坐骑,6房间扣费,7计时扣费,8发送红包,9抢红包,10开通守护,11注册奖励,12礼物中奖,13奖池中奖,14缴纳保证金,15退还保证金,16转盘游戏,17转盘中奖,18购买靓号,19游戏下注,20游戏退还,21每日任务奖励',
  `uid` int(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `touid` int(20) NOT NULL DEFAULT 0 COMMENT '对方ID',
  `giftid` int(20) NOT NULL DEFAULT 0 COMMENT '行为对应ID',
  `giftcount` int(20) NOT NULL DEFAULT 0 COMMENT '数量',
  `totalcoin` int(20) NOT NULL DEFAULT 0 COMMENT '总价',
  `showid` int(12) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `addtime` int(12) NOT NULL DEFAULT 0 COMMENT '添加时间',
  `mark` tinyint(1) NOT NULL DEFAULT 0 COMMENT '标识，1表示热门礼物，2表示守护礼物',
  `ispack` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否为背包礼物 0 否 1 是',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_uid_addtime`(`action`, `uid`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 218 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_coinrecord
-- ----------------------------
INSERT INTO `cmf_user_coinrecord` VALUES (1, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644978387, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (2, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644978419, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (3, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644979509, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (4, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644979526, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (5, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644979565, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (6, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644979771, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (7, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644979771, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (8, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644980241, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (9, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644980306, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (10, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981568, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (11, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981569, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (12, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981569, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (13, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981569, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (14, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981570, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (15, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644981570, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (16, 0, 1, 3, 2, 77, 1, 1000, 1644823331, 1644982120, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (17, 0, 1, 4, 2, 76, 1, 500, 1644823331, 1644982135, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (18, 0, 1, 3, 2, 76, 1, 500, 1644823331, 1644982145, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (19, 0, 1, 4, 2, 77, 1, 1000, 1644823331, 1644982908, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (20, 0, 1, 4, 2, 77, 1, 1000, 1644823331, 1644982914, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (21, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982918, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (22, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982919, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (23, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982920, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (24, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982920, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (25, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982920, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (26, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982920, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (27, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982920, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (28, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (29, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (30, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (31, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (32, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (33, 0, 1, 4, 2, 21, 1, 1, 1644823331, 1644982924, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (34, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982926, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (35, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982926, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (36, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982926, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (37, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982926, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (38, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (39, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (40, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (41, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (42, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (43, 0, 1, 4, 2, 22, 1, 2, 1644823331, 1644982927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (44, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982929, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (45, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982929, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (46, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (47, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (48, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (49, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (50, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (51, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982930, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (52, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982931, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (53, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982931, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (54, 0, 1, 4, 2, 11, 1, 50, 1644823331, 1644982931, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (55, 0, 1, 4, 2, 10, 1, 1000, 1644823331, 1644982932, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (56, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982972, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (57, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982974, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (58, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982975, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (59, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982976, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (60, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982978, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (61, 0, 2, 4, 2, 0, 1, 10, 1644823331, 1644982979, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (62, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994549, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (63, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (64, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (65, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (66, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (67, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (68, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994550, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (69, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994551, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (70, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994551, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (71, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994551, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (72, 0, 1, 3, 2, 27, 1, 27, 1644823331, 1644994551, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (73, 0, 1, 3, 2, 26, 1, 26, 1644823331, 1644994552, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (74, 0, 1, 3, 2, 26, 1, 26, 1644823331, 1644994552, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (75, 0, 1, 3, 2, 26, 1, 26, 1644823331, 1644994552, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (76, 0, 1, 3, 2, 26, 1, 26, 1644823331, 1644994552, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (77, 0, 1, 3, 2, 26, 1, 26, 1644823331, 1644994553, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (78, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997572, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (79, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997573, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (80, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997573, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (81, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997573, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (82, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997573, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (83, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997573, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (84, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997576, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (85, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997576, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (86, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997576, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (87, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997576, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (88, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997577, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (89, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997577, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (90, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997577, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (91, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997582, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (92, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997582, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (93, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997582, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (94, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997582, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (95, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997582, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (96, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997583, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (97, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997583, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (98, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997583, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (99, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997583, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (100, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997584, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (101, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997584, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (102, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997584, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (103, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997584, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (104, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997586, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (105, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997586, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (106, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997586, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (107, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997587, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (108, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997587, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (109, 0, 1, 4, 3, 22, 1, 2, 1644996774, 1644997587, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (110, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997589, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (111, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997589, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (112, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997589, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (113, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997589, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (114, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997589, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (115, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (116, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (117, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (118, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (119, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (120, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997590, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (121, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997591, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (122, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (123, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (124, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (125, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (126, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (127, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997592, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (128, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997593, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (129, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997593, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (130, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997594, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (131, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997594, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (132, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997594, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (133, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997594, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (134, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997594, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (135, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997595, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (136, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997595, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (137, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997597, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (138, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997597, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (139, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997597, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (140, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997597, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (141, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997598, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (142, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997598, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (143, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997604, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (144, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997604, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (145, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997604, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (146, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997605, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (147, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997609, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (148, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997609, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (149, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (150, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (151, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (152, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (153, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (154, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997610, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (155, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997611, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (156, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997611, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (157, 0, 1, 4, 3, 26, 1, 26, 1644996774, 1644997611, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (158, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004533, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (159, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004533, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (160, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004534, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (161, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004534, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (162, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004534, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (163, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004534, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (164, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004534, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (165, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004535, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (166, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004535, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (167, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004536, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (168, 0, 1, 3, 2, 27, 10, 270, 1644823331, 1645004536, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (169, 0, 1, 9, 7, 77, 1, 1000, 1645152871, 1645152922, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (170, 0, 2, 9, 7, 0, 1, 10, 1645152871, 1645152937, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (171, 0, 1, 9, 7, 119, 1, 520, 1645152871, 1645153013, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (172, 0, 1, 9, 7, 22, 1, 2, 1645152871, 1645153020, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (173, 0, 6, 7, 9, 0, 0, 20, 1645153741, 1645153768, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (174, 0, 1, 7, 9, 76, 1, 500, 1645153741, 1645153773, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (175, 0, 7, 7, 9, 0, 0, 20, 1645153908, 1645153915, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (176, 0, 7, 7, 9, 0, 0, 20, 1645153908, 1645153927, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (177, 0, 1, 7, 9, 76, 1, 500, 1645153908, 1645153936, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (178, 0, 1, 7, 2, 119, 1, 520, 1644823331, 1645154539, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (179, 1, 11, 10, 10, 0, 1, 9999, 0, 1645175068, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (180, 0, 1, 7, 10, 7, 1, 300, 1645175642, 1645175806, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (181, 0, 1, 7, 10, 21, 1, 1, 1645175642, 1645175815, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (182, 0, 2, 7, 10, 0, 1, 10, 1645175642, 1645175846, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (183, 1, 11, 11, 11, 0, 1, 9999, 0, 1645230563, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (184, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236134, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (185, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236134, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (186, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236134, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (187, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236135, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (188, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236135, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (189, 0, 1, 10, 2, 14, 1, 50, 1644823331, 1645236135, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (190, 0, 1, 10, 2, 15, 1, 100, 1644823331, 1645236136, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (191, 0, 1, 10, 2, 15, 1, 100, 1644823331, 1645236136, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (192, 0, 1, 10, 2, 15, 1, 100, 1644823331, 1645236136, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (193, 0, 1, 10, 2, 15, 1, 100, 1644823331, 1645236137, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (194, 0, 1, 10, 2, 15, 1, 100, 1644823331, 1645236137, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (195, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236138, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (196, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236138, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (197, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236138, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (198, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236139, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (199, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236139, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (200, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236139, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (201, 0, 1, 10, 2, 16, 1, 10, 1644823331, 1645236139, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (202, 0, 1, 10, 2, 21, 1, 1, 1644823331, 1645236142, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (203, 0, 1, 10, 2, 21, 1, 1, 1644823331, 1645236143, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (204, 0, 1, 10, 2, 21, 1, 1, 1644823331, 1645236143, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (205, 0, 1, 10, 2, 21, 1, 1, 1644823331, 1645236143, 1, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (206, 0, 1, 10, 2, 22, 1, 2, 1644823331, 1645236144, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (207, 0, 1, 10, 2, 22, 1, 2, 1644823331, 1645236144, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (208, 0, 1, 10, 2, 22, 1, 2, 1644823331, 1645236145, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (209, 0, 1, 10, 2, 22, 1, 2, 1644823331, 1645236145, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (210, 0, 1, 10, 2, 22, 1, 2, 1644823331, 1645236145, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (211, 0, 1, 10, 2, 7, 1, 300, 1644823331, 1645236146, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (212, 0, 2, 10, 2, 0, 1, 10, 1644823331, 1645238118, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (213, 0, 2, 10, 2, 0, 1, 10, 1644823331, 1645238126, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (214, 0, 1, 8, 2, 77, 1, 1000, 1644823331, 1645251170, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (215, 0, 2, 7, 8, 0, 1, 10, 1645254228, 1645254327, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (216, 0, 1, 11, 2, 77, 1, 1000, 1644823331, 1645264689, 0, 0);
INSERT INTO `cmf_user_coinrecord` VALUES (217, 0, 1, 11, 2, 22, 1, 2, 1644823331, 1645264691, 0, 0);

-- ----------------------------
-- Table structure for cmf_user_pushid
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_pushid`;
CREATE TABLE `cmf_user_pushid`  (
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户ID',
  `pushid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户对应极光registration_id',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_sign
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_sign`;
CREATE TABLE `cmf_user_sign`  (
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `bonus_day` int(11) NOT NULL DEFAULT 0 COMMENT '登录天数',
  `bonus_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `count_day` int(11) NOT NULL DEFAULT 0 COMMENT '连续登陆天数',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_super
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_super`;
CREATE TABLE `cmf_user_super`  (
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `addtime` int(11) NOT NULL DEFAULT 0 COMMENT '时间',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_user_token
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_token`;
CREATE TABLE `cmf_user_token`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户id',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT ' 过期时间',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'token',
  `device_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '设备类型;mobile,android,iphone,ipad,web,pc,mac,wxapp',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户客户端登录 token 表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_token
-- ----------------------------
INSERT INTO `cmf_user_token` VALUES (1, 1, 1660011635, 1644459635, '9c16dd8f5d79abda8de862b8ba2e49bf5e3452304df63d2672a05acdda0b05cf', 'web');
INSERT INTO `cmf_user_token` VALUES (2, 3, 1671090334, 1645170334, 'b9d2d78ece5474a5d131830192f803aa', '');
INSERT INTO `cmf_user_token` VALUES (3, 4, 1671328760, 1645408760, '6d02a2f746f520642a126b03d227264a', '');
INSERT INTO `cmf_user_token` VALUES (4, 5, 1671091570, 1645171570, '14f89001a5a04722c16ab89e1979753b', '');
INSERT INTO `cmf_user_token` VALUES (5, 6, 1671010263, 1645090263, '9bb146b513e8885d6b11cbec0095d939', '');
INSERT INTO `cmf_user_token` VALUES (6, 9, 1671071258, 1645151258, '913ffb6ee20ccdcb9ff495c00da1121b', '');
INSERT INTO `cmf_user_token` VALUES (7, 7, 1671174226, 1645254226, '73d585994a2b3a7f972a88dbbe88a9b6', '');
INSERT INTO `cmf_user_token` VALUES (8, 10, 1671158096, 1645238096, '05c10c56986f43222101dd62c3041f17', '');
INSERT INTO `cmf_user_token` VALUES (9, 11, 1671183655, 1645263655, 'f54c6a071c778006bdceb2945e56a2cf', '');
INSERT INTO `cmf_user_token` VALUES (10, 8, 1671169138, 1645249138, '6a87049f9892f9638a3af7fb5c5c0487', '');

-- ----------------------------
-- Table structure for cmf_user_voterecord
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_voterecord`;
CREATE TABLE `cmf_user_voterecord`  (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支类型,0支出，1收入',
  `action` tinyint(1) NOT NULL DEFAULT 0 COMMENT '收支行为,1收礼物2弹幕3分销收益4家族长收益6房间收费7计时收费10守护',
  `uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户ID',
  `fromid` bigint(20) NOT NULL DEFAULT 0 COMMENT '来源用户ID',
  `actionid` bigint(20) NOT NULL DEFAULT 0 COMMENT '行为对应ID',
  `nums` bigint(20) NOT NULL DEFAULT 0 COMMENT '数量',
  `total` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '总价',
  `showid` bigint(20) NOT NULL DEFAULT 0 COMMENT '直播标识',
  `votes` decimal(20, 2) NOT NULL DEFAULT 0.00 COMMENT '收益映票',
  `addtime` bigint(20) NOT NULL DEFAULT 0 COMMENT '添加时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_uid_addtime`(`action`, `uid`, `addtime`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 216 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cmf_user_voterecord
-- ----------------------------
INSERT INTO `cmf_user_voterecord` VALUES (1, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644978387);
INSERT INTO `cmf_user_voterecord` VALUES (2, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644978419);
INSERT INTO `cmf_user_voterecord` VALUES (3, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644979509);
INSERT INTO `cmf_user_voterecord` VALUES (4, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644979526);
INSERT INTO `cmf_user_voterecord` VALUES (5, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644979565);
INSERT INTO `cmf_user_voterecord` VALUES (6, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644979771);
INSERT INTO `cmf_user_voterecord` VALUES (7, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644979771);
INSERT INTO `cmf_user_voterecord` VALUES (8, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644980241);
INSERT INTO `cmf_user_voterecord` VALUES (9, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644980306);
INSERT INTO `cmf_user_voterecord` VALUES (10, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981569);
INSERT INTO `cmf_user_voterecord` VALUES (11, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981569);
INSERT INTO `cmf_user_voterecord` VALUES (12, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981569);
INSERT INTO `cmf_user_voterecord` VALUES (13, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981569);
INSERT INTO `cmf_user_voterecord` VALUES (14, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981570);
INSERT INTO `cmf_user_voterecord` VALUES (15, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644981570);
INSERT INTO `cmf_user_voterecord` VALUES (16, 1, 1, 2, 3, 77, 1, 1000.00, 1644823331, 1000.00, 1644982120);
INSERT INTO `cmf_user_voterecord` VALUES (17, 1, 1, 2, 4, 76, 1, 500.00, 1644823331, 500.00, 1644982135);
INSERT INTO `cmf_user_voterecord` VALUES (18, 1, 1, 2, 3, 76, 1, 500.00, 1644823331, 500.00, 1644982145);
INSERT INTO `cmf_user_voterecord` VALUES (19, 1, 1, 2, 4, 77, 1, 1000.00, 1644823331, 1000.00, 1644982908);
INSERT INTO `cmf_user_voterecord` VALUES (20, 1, 1, 2, 4, 77, 1, 1000.00, 1644823331, 1000.00, 1644982914);
INSERT INTO `cmf_user_voterecord` VALUES (21, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982918);
INSERT INTO `cmf_user_voterecord` VALUES (22, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982919);
INSERT INTO `cmf_user_voterecord` VALUES (23, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982920);
INSERT INTO `cmf_user_voterecord` VALUES (24, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982920);
INSERT INTO `cmf_user_voterecord` VALUES (25, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982920);
INSERT INTO `cmf_user_voterecord` VALUES (26, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982920);
INSERT INTO `cmf_user_voterecord` VALUES (27, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982920);
INSERT INTO `cmf_user_voterecord` VALUES (28, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (29, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (30, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (31, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (32, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (33, 1, 1, 2, 4, 21, 1, 1.00, 1644823331, 1.00, 1644982924);
INSERT INTO `cmf_user_voterecord` VALUES (34, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982926);
INSERT INTO `cmf_user_voterecord` VALUES (35, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982926);
INSERT INTO `cmf_user_voterecord` VALUES (36, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982926);
INSERT INTO `cmf_user_voterecord` VALUES (37, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (38, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (39, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (40, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (41, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (42, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (43, 1, 1, 2, 4, 22, 1, 2.00, 1644823331, 2.00, 1644982927);
INSERT INTO `cmf_user_voterecord` VALUES (44, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982929);
INSERT INTO `cmf_user_voterecord` VALUES (45, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982929);
INSERT INTO `cmf_user_voterecord` VALUES (46, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (47, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (48, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (49, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (50, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (51, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982930);
INSERT INTO `cmf_user_voterecord` VALUES (52, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982931);
INSERT INTO `cmf_user_voterecord` VALUES (53, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982931);
INSERT INTO `cmf_user_voterecord` VALUES (54, 1, 1, 2, 4, 11, 1, 50.00, 1644823331, 50.00, 1644982931);
INSERT INTO `cmf_user_voterecord` VALUES (55, 1, 1, 2, 4, 10, 1, 1000.00, 1644823331, 1000.00, 1644982932);
INSERT INTO `cmf_user_voterecord` VALUES (56, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982972);
INSERT INTO `cmf_user_voterecord` VALUES (57, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982974);
INSERT INTO `cmf_user_voterecord` VALUES (58, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982975);
INSERT INTO `cmf_user_voterecord` VALUES (59, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982976);
INSERT INTO `cmf_user_voterecord` VALUES (60, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982978);
INSERT INTO `cmf_user_voterecord` VALUES (61, 1, 2, 2, 4, 0, 1, 10.00, 1644823331, 10.00, 1644982979);
INSERT INTO `cmf_user_voterecord` VALUES (62, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994549);
INSERT INTO `cmf_user_voterecord` VALUES (63, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (64, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (65, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (66, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (67, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (68, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994550);
INSERT INTO `cmf_user_voterecord` VALUES (69, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994551);
INSERT INTO `cmf_user_voterecord` VALUES (70, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994551);
INSERT INTO `cmf_user_voterecord` VALUES (71, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994551);
INSERT INTO `cmf_user_voterecord` VALUES (72, 1, 1, 2, 3, 27, 1, 27.00, 1644823331, 27.00, 1644994551);
INSERT INTO `cmf_user_voterecord` VALUES (73, 1, 1, 2, 3, 26, 1, 26.00, 1644823331, 26.00, 1644994552);
INSERT INTO `cmf_user_voterecord` VALUES (74, 1, 1, 2, 3, 26, 1, 26.00, 1644823331, 26.00, 1644994552);
INSERT INTO `cmf_user_voterecord` VALUES (75, 1, 1, 2, 3, 26, 1, 26.00, 1644823331, 26.00, 1644994552);
INSERT INTO `cmf_user_voterecord` VALUES (76, 1, 1, 2, 3, 26, 1, 26.00, 1644823331, 26.00, 1644994552);
INSERT INTO `cmf_user_voterecord` VALUES (77, 1, 1, 2, 3, 26, 1, 26.00, 1644823331, 26.00, 1644994553);
INSERT INTO `cmf_user_voterecord` VALUES (78, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997572);
INSERT INTO `cmf_user_voterecord` VALUES (79, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997573);
INSERT INTO `cmf_user_voterecord` VALUES (80, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997573);
INSERT INTO `cmf_user_voterecord` VALUES (81, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997573);
INSERT INTO `cmf_user_voterecord` VALUES (82, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997573);
INSERT INTO `cmf_user_voterecord` VALUES (83, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997573);
INSERT INTO `cmf_user_voterecord` VALUES (84, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997576);
INSERT INTO `cmf_user_voterecord` VALUES (85, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997576);
INSERT INTO `cmf_user_voterecord` VALUES (86, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997576);
INSERT INTO `cmf_user_voterecord` VALUES (87, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997576);
INSERT INTO `cmf_user_voterecord` VALUES (88, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997577);
INSERT INTO `cmf_user_voterecord` VALUES (89, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997577);
INSERT INTO `cmf_user_voterecord` VALUES (90, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997577);
INSERT INTO `cmf_user_voterecord` VALUES (91, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997582);
INSERT INTO `cmf_user_voterecord` VALUES (92, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997582);
INSERT INTO `cmf_user_voterecord` VALUES (93, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997582);
INSERT INTO `cmf_user_voterecord` VALUES (94, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997582);
INSERT INTO `cmf_user_voterecord` VALUES (95, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997582);
INSERT INTO `cmf_user_voterecord` VALUES (96, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997583);
INSERT INTO `cmf_user_voterecord` VALUES (97, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997583);
INSERT INTO `cmf_user_voterecord` VALUES (98, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997583);
INSERT INTO `cmf_user_voterecord` VALUES (99, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997583);
INSERT INTO `cmf_user_voterecord` VALUES (100, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997584);
INSERT INTO `cmf_user_voterecord` VALUES (101, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997584);
INSERT INTO `cmf_user_voterecord` VALUES (102, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997584);
INSERT INTO `cmf_user_voterecord` VALUES (103, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997584);
INSERT INTO `cmf_user_voterecord` VALUES (104, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997586);
INSERT INTO `cmf_user_voterecord` VALUES (105, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997586);
INSERT INTO `cmf_user_voterecord` VALUES (106, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997587);
INSERT INTO `cmf_user_voterecord` VALUES (107, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997587);
INSERT INTO `cmf_user_voterecord` VALUES (108, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997587);
INSERT INTO `cmf_user_voterecord` VALUES (109, 1, 1, 3, 4, 22, 1, 2.00, 1644996774, 2.00, 1644997587);
INSERT INTO `cmf_user_voterecord` VALUES (110, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997589);
INSERT INTO `cmf_user_voterecord` VALUES (111, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997589);
INSERT INTO `cmf_user_voterecord` VALUES (112, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997589);
INSERT INTO `cmf_user_voterecord` VALUES (113, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997589);
INSERT INTO `cmf_user_voterecord` VALUES (114, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997589);
INSERT INTO `cmf_user_voterecord` VALUES (115, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (116, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (117, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (118, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (119, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (120, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997590);
INSERT INTO `cmf_user_voterecord` VALUES (121, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997591);
INSERT INTO `cmf_user_voterecord` VALUES (122, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (123, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (124, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (125, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (126, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (127, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997592);
INSERT INTO `cmf_user_voterecord` VALUES (128, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997593);
INSERT INTO `cmf_user_voterecord` VALUES (129, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997593);
INSERT INTO `cmf_user_voterecord` VALUES (130, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997594);
INSERT INTO `cmf_user_voterecord` VALUES (131, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997594);
INSERT INTO `cmf_user_voterecord` VALUES (132, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997594);
INSERT INTO `cmf_user_voterecord` VALUES (133, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997594);
INSERT INTO `cmf_user_voterecord` VALUES (134, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997594);
INSERT INTO `cmf_user_voterecord` VALUES (135, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997595);
INSERT INTO `cmf_user_voterecord` VALUES (136, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997595);
INSERT INTO `cmf_user_voterecord` VALUES (137, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997597);
INSERT INTO `cmf_user_voterecord` VALUES (138, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997597);
INSERT INTO `cmf_user_voterecord` VALUES (139, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997597);
INSERT INTO `cmf_user_voterecord` VALUES (140, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997597);
INSERT INTO `cmf_user_voterecord` VALUES (141, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997598);
INSERT INTO `cmf_user_voterecord` VALUES (142, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997598);
INSERT INTO `cmf_user_voterecord` VALUES (143, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997604);
INSERT INTO `cmf_user_voterecord` VALUES (144, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997604);
INSERT INTO `cmf_user_voterecord` VALUES (145, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997605);
INSERT INTO `cmf_user_voterecord` VALUES (146, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997605);
INSERT INTO `cmf_user_voterecord` VALUES (147, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997609);
INSERT INTO `cmf_user_voterecord` VALUES (148, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997609);
INSERT INTO `cmf_user_voterecord` VALUES (149, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (150, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (151, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (152, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (153, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (154, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997610);
INSERT INTO `cmf_user_voterecord` VALUES (155, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997611);
INSERT INTO `cmf_user_voterecord` VALUES (156, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997611);
INSERT INTO `cmf_user_voterecord` VALUES (157, 1, 1, 3, 4, 26, 1, 26.00, 1644996774, 26.00, 1644997611);
INSERT INTO `cmf_user_voterecord` VALUES (158, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004533);
INSERT INTO `cmf_user_voterecord` VALUES (159, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004533);
INSERT INTO `cmf_user_voterecord` VALUES (160, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004534);
INSERT INTO `cmf_user_voterecord` VALUES (161, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004534);
INSERT INTO `cmf_user_voterecord` VALUES (162, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004534);
INSERT INTO `cmf_user_voterecord` VALUES (163, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004534);
INSERT INTO `cmf_user_voterecord` VALUES (164, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004534);
INSERT INTO `cmf_user_voterecord` VALUES (165, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004535);
INSERT INTO `cmf_user_voterecord` VALUES (166, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004535);
INSERT INTO `cmf_user_voterecord` VALUES (167, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004536);
INSERT INTO `cmf_user_voterecord` VALUES (168, 1, 1, 2, 3, 27, 10, 270.00, 1644823331, 270.00, 1645004536);
INSERT INTO `cmf_user_voterecord` VALUES (169, 1, 1, 7, 9, 77, 1, 1000.00, 1645152871, 1000.00, 1645152922);
INSERT INTO `cmf_user_voterecord` VALUES (170, 1, 2, 7, 9, 0, 1, 10.00, 1645152871, 10.00, 1645152937);
INSERT INTO `cmf_user_voterecord` VALUES (171, 1, 1, 7, 9, 119, 1, 520.00, 1645152871, 520.00, 1645153013);
INSERT INTO `cmf_user_voterecord` VALUES (172, 1, 1, 7, 9, 22, 1, 2.00, 1645152871, 2.00, 1645153020);
INSERT INTO `cmf_user_voterecord` VALUES (173, 1, 6, 9, 7, 0, 0, 20.00, 1645153741, 20.00, 1645153768);
INSERT INTO `cmf_user_voterecord` VALUES (174, 1, 1, 9, 7, 76, 1, 500.00, 1645153741, 500.00, 1645153773);
INSERT INTO `cmf_user_voterecord` VALUES (175, 1, 7, 9, 7, 0, 0, 20.00, 1645153908, 20.00, 1645153915);
INSERT INTO `cmf_user_voterecord` VALUES (176, 1, 7, 9, 7, 0, 0, 20.00, 1645153908, 20.00, 1645153927);
INSERT INTO `cmf_user_voterecord` VALUES (177, 1, 1, 9, 7, 76, 1, 500.00, 1645153908, 500.00, 1645153936);
INSERT INTO `cmf_user_voterecord` VALUES (178, 1, 1, 2, 7, 119, 1, 520.00, 1644823331, 520.00, 1645154539);
INSERT INTO `cmf_user_voterecord` VALUES (179, 1, 1, 10, 7, 7, 1, 300.00, 1645175642, 300.00, 1645175806);
INSERT INTO `cmf_user_voterecord` VALUES (180, 1, 1, 10, 7, 21, 1, 1.00, 1645175642, 1.00, 1645175815);
INSERT INTO `cmf_user_voterecord` VALUES (181, 1, 2, 10, 7, 0, 1, 10.00, 1645175642, 10.00, 1645175846);
INSERT INTO `cmf_user_voterecord` VALUES (182, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236134);
INSERT INTO `cmf_user_voterecord` VALUES (183, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236134);
INSERT INTO `cmf_user_voterecord` VALUES (184, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236134);
INSERT INTO `cmf_user_voterecord` VALUES (185, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236135);
INSERT INTO `cmf_user_voterecord` VALUES (186, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236135);
INSERT INTO `cmf_user_voterecord` VALUES (187, 1, 1, 2, 10, 14, 1, 50.00, 1644823331, 50.00, 1645236135);
INSERT INTO `cmf_user_voterecord` VALUES (188, 1, 1, 2, 10, 15, 1, 100.00, 1644823331, 100.00, 1645236136);
INSERT INTO `cmf_user_voterecord` VALUES (189, 1, 1, 2, 10, 15, 1, 100.00, 1644823331, 100.00, 1645236136);
INSERT INTO `cmf_user_voterecord` VALUES (190, 1, 1, 2, 10, 15, 1, 100.00, 1644823331, 100.00, 1645236137);
INSERT INTO `cmf_user_voterecord` VALUES (191, 1, 1, 2, 10, 15, 1, 100.00, 1644823331, 100.00, 1645236137);
INSERT INTO `cmf_user_voterecord` VALUES (192, 1, 1, 2, 10, 15, 1, 100.00, 1644823331, 100.00, 1645236137);
INSERT INTO `cmf_user_voterecord` VALUES (193, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236138);
INSERT INTO `cmf_user_voterecord` VALUES (194, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236138);
INSERT INTO `cmf_user_voterecord` VALUES (195, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236138);
INSERT INTO `cmf_user_voterecord` VALUES (196, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236139);
INSERT INTO `cmf_user_voterecord` VALUES (197, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236139);
INSERT INTO `cmf_user_voterecord` VALUES (198, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236139);
INSERT INTO `cmf_user_voterecord` VALUES (199, 1, 1, 2, 10, 16, 1, 10.00, 1644823331, 10.00, 1645236139);
INSERT INTO `cmf_user_voterecord` VALUES (200, 1, 1, 2, 10, 21, 1, 1.00, 1644823331, 1.00, 1645236142);
INSERT INTO `cmf_user_voterecord` VALUES (201, 1, 1, 2, 10, 21, 1, 1.00, 1644823331, 1.00, 1645236143);
INSERT INTO `cmf_user_voterecord` VALUES (202, 1, 1, 2, 10, 21, 1, 1.00, 1644823331, 1.00, 1645236143);
INSERT INTO `cmf_user_voterecord` VALUES (203, 1, 1, 2, 10, 21, 1, 1.00, 1644823331, 1.00, 1645236143);
INSERT INTO `cmf_user_voterecord` VALUES (204, 1, 1, 2, 10, 22, 1, 2.00, 1644823331, 2.00, 1645236144);
INSERT INTO `cmf_user_voterecord` VALUES (205, 1, 1, 2, 10, 22, 1, 2.00, 1644823331, 2.00, 1645236145);
INSERT INTO `cmf_user_voterecord` VALUES (206, 1, 1, 2, 10, 22, 1, 2.00, 1644823331, 2.00, 1645236145);
INSERT INTO `cmf_user_voterecord` VALUES (207, 1, 1, 2, 10, 22, 1, 2.00, 1644823331, 2.00, 1645236145);
INSERT INTO `cmf_user_voterecord` VALUES (208, 1, 1, 2, 10, 22, 1, 2.00, 1644823331, 2.00, 1645236145);
INSERT INTO `cmf_user_voterecord` VALUES (209, 1, 1, 2, 10, 7, 1, 300.00, 1644823331, 300.00, 1645236146);
INSERT INTO `cmf_user_voterecord` VALUES (210, 1, 2, 2, 10, 0, 1, 10.00, 1644823331, 10.00, 1645238118);
INSERT INTO `cmf_user_voterecord` VALUES (211, 1, 2, 2, 10, 0, 1, 10.00, 1644823331, 10.00, 1645238126);
INSERT INTO `cmf_user_voterecord` VALUES (212, 1, 1, 2, 8, 77, 1, 1000.00, 1644823331, 1000.00, 1645251170);
INSERT INTO `cmf_user_voterecord` VALUES (213, 1, 2, 8, 7, 0, 1, 10.00, 1645254228, 10.00, 1645254327);
INSERT INTO `cmf_user_voterecord` VALUES (214, 1, 1, 2, 11, 77, 1, 1000.00, 1644823331, 1000.00, 1645264689);
INSERT INTO `cmf_user_voterecord` VALUES (215, 1, 1, 2, 11, 22, 1, 2.00, 1644823331, 2.00, 1645264691);

-- ----------------------------
-- Table structure for cmf_user_zombie
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_zombie`;
CREATE TABLE `cmf_user_zombie`  (
  `uid` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for cmf_verification_code
-- ----------------------------
DROP TABLE IF EXISTS `cmf_verification_code`;
CREATE TABLE `cmf_verification_code`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '表id',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '当天已经发送成功的次数',
  `send_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后发送成功时间',
  `expire_time` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '验证码过期时间',
  `code` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '最后发送成功的验证码',
  `account` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机号或者邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '手机邮箱数字验证码表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Function structure for getDistance
-- ----------------------------
DROP FUNCTION IF EXISTS `getDistance`;
delimiter ;;
CREATE FUNCTION `getDistance`(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT)
 RETURNS float
  DETERMINISTIC
BEGIN
    RETURN ROUND(6378.138 * 2 * ASIN(SQRT(POW(SIN((lat1 * PI() / 180 - lat2 * PI() / 180) / 2), 2)
           + COS(lat1 * PI() / 180) * COS(lat2 * PI() / 180)
           * POW(SIN(( lon1 * PI() / 180 - lon2 * PI() / 180 ) / 2),2))),2);
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
